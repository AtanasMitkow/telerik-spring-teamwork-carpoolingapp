package com.example.carpooling.config.jwt_config;

import com.example.carpooling.exceptions.InvalidTokenException;
import com.example.carpooling.models.utility.JWTBlacklist;
import com.example.carpooling.models.utility.Role;
import com.example.carpooling.repositories.JWTBlacklistRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

    private String secretKey;
    private long validityInMilliseconds = 3600000; // 1h
    private MyUserDetails myUserDetails;
    private JWTBlacklistRepository jwtBlacklistRepository;

    @Autowired
    public JwtTokenProvider(MyUserDetails myUserDetails, Environment environment, JWTBlacklistRepository jwtBlacklistRepository) {
        this.myUserDetails = myUserDetails;
        this.jwtBlacklistRepository = jwtBlacklistRepository;
        this.secretKey = environment.getProperty("jwt.secret");
        String expiration = environment.getProperty("jwt.expiration");
        if (expiration != null) this.validityInMilliseconds = Long.parseLong(expiration);
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String username, List<Role> roles) {

        Claims claims = Jwts.claims().setSubject(username);
        claims.put("auth", roles.stream().map(s -> new SimpleGrantedAuthority(s.getAuthority())).filter(Objects::nonNull).collect(Collectors.toList()));

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    Authentication getAuthentication(String token) {
        UserDetails userDetails = myUserDetails.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies == null) return null;
        Cookie authCookie = Arrays.stream(cookies).filter(c -> c.getName().equals("auth")).findFirst().orElse(null);
        if (authCookie != null)
            return authCookie.getValue();
        return null;
    }

    boolean validateToken(String token) {
        try {
            JWTBlacklist blacklistedToken = jwtBlacklistRepository.getByToken(token);
            if (blacklistedToken != null) throw new IllegalArgumentException();
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new InvalidTokenException("Expired or invalid token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

