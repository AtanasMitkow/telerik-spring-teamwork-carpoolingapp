package com.example.carpooling.DTO;

public class EditCarDTO {
    private long id;
    private String model;
    private int seats;
    private boolean hasAirConditioner;
    private int yearMade;
    private String carImages;

    public EditCarDTO() {

    }

    public EditCarDTO(long id, String model, int seats, boolean hasAirConditioner, int yearMade, String carImages) {
        this.id = id;
        this.model = model;
        this.seats = seats;
        this.hasAirConditioner = hasAirConditioner;
        this.yearMade = yearMade;
        this.carImages = carImages;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public boolean isHasAirConditioner() {
        return hasAirConditioner;
    }

    public void setHasAirConditioner(boolean hasAirConditioner) {
        this.hasAirConditioner = hasAirConditioner;
    }

    public int getYearMade() {
        return yearMade;
    }

    public void setYearMade(int yearMade) {
        this.yearMade = yearMade;
    }

    public String getCarImages() {
        return carImages;
    }

    public void setCarImages(String carImages) {
        this.carImages = carImages;
    }
}
