package com.example.carpooling.DTO;

public class CreateCarDTO {
    private String model;
    private int seats;
    private boolean hasAirCondition;
    private int yearMade;

    public CreateCarDTO(String model, int seats, boolean hasAirCondition, int yearMade) {
        this.model = model;
        this.seats = seats;
        this.hasAirCondition = hasAirCondition;
        this.yearMade = yearMade;
    }

    public CreateCarDTO() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public boolean isHasAirCondition() {
        return hasAirCondition;
    }

    public void setHasAirCondition(boolean hasAirCondition) {
        this.hasAirCondition = hasAirCondition;
    }

    public int getYearMade() {
        return yearMade;
    }

    public void setYearMade(int yearMade) {
        this.yearMade = yearMade;
    }

}

