package com.example.carpooling.DTO;

public class ImageDTO {
    private long id;
    private String url;

    public ImageDTO(long id, String url) {
        this.id = id;
        this.url = url;
    }

    public ImageDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
