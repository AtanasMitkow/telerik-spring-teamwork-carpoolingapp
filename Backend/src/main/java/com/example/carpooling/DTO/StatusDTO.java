package com.example.carpooling.DTO;

public class StatusDTO {

    private boolean amIPassenger;
    private String status;

    public StatusDTO() {
    }

    public StatusDTO(boolean amIPassenger, String status) {
        this.amIPassenger = amIPassenger;
        this.status = status;
    }

    public boolean isAmIPassenger() {
        return amIPassenger;
    }

    public void setAmIPassenger(boolean amIPassenger) {
        this.amIPassenger = amIPassenger;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
