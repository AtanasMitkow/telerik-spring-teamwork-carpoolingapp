package com.example.carpooling.DTO;

public class CreateTripDTO {
    private long carID;
    private String message;
    private String departureTime;
    private String origin;
    private String destination;
    private int availablePlaces;
    private boolean allowsSmoking;
    private boolean allowsPets;
    private boolean allowsLuggage;
    private int estimatedTime;
    private int length;

    public CreateTripDTO() {
    }

    public CreateTripDTO(long carID, String message, String departureTime, String origin, String destination,
                         int availablePlaces, boolean allowsSmoking, boolean allowsPets, boolean allowsLuggage,
                         int estimatedTime, int length) {
        this.carID = carID;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.allowsSmoking = allowsSmoking;
        this.allowsPets = allowsPets;
        this.allowsLuggage = allowsLuggage;
        this.estimatedTime = estimatedTime;
        this.length = length;
    }

    public long getCarID() {
        return carID;
    }

    public void setCarID(long carID) {
        this.carID = carID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public boolean isAllowsSmoking() {
        return allowsSmoking;
    }

    public void setAllowsSmoking(boolean allowsSmoking) {
        this.allowsSmoking = allowsSmoking;
    }

    public boolean isAllowsPets() {
        return allowsPets;
    }

    public void setAllowsPets(boolean allowsPets) {
        this.allowsPets = allowsPets;
    }

    public boolean isAllowsLuggage() {
        return allowsLuggage;
    }

    public void setAllowsLuggage(boolean allowsLuggage) {
        this.allowsLuggage = allowsLuggage;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
