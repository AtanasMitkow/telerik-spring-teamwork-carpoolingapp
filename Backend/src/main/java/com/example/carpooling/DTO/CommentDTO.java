package com.example.carpooling.DTO;

import java.util.Date;

public class CommentDTO {

    private long id;
    private String message;
    private UserDTO author;
    private Date createdAt;

    public CommentDTO() {
    }

    public CommentDTO(long id, String message, UserDTO author,Date createAt) {
        this.id = id;
        this.message = message;
        this.author = author;
        this.createdAt = createAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createAt) {
        this.createdAt = createAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserDTO author) {
        this.author = author;
    }
}
