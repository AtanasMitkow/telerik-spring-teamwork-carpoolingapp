package com.example.carpooling.DTO;

public class CreateCommentDTO {
    private String message;

    public CreateCommentDTO() {
    }

    public CreateCommentDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
