package com.example.carpooling.DTO;

import com.example.carpooling.models.Car;

import java.util.Date;

public class TripDTO {
    private long id;
    private UserDTO driver;
    private Car car;
    private String message;
    private Date departureTime;
    private String origin;
    private String destination;
    private int availablePlaces;
    private String status;
    private boolean allowsSmoking;
    private boolean allowsPets;
    private boolean allowsLuggage;
    private int estimatedTime;
    private int length;

    public TripDTO() {
    }

    public TripDTO(long id, UserDTO driver, Car car, String message, Date departureTime,
                   String origin, String destination, int availablePlaces, String status,
                   boolean allowsSmoking, boolean allowsPets, boolean allowsLuggage,
                   int estimatedTime, int length) {
        this.id = id;
        this.driver = driver;
        this.car = car;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.status = status;
        this.allowsSmoking = allowsSmoking;
        this.allowsPets = allowsPets;
        this.allowsLuggage = allowsLuggage;
        this.estimatedTime = estimatedTime;
        this.length = length;
    }

    public long getId() {
        return id;
    }

    public UserDTO getDriver() {
        return driver;
    }

    public Car getCar() {
        return car;
    }

    public String getMessage() {
        return message;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public String getStatus() {
        return status;
    }

    public boolean isAllowsSmoking() {
        return allowsSmoking;
    }

    public boolean isAllowsPets() {
        return allowsPets;
    }

    public boolean isAllowsLuggage() {
        return allowsLuggage;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDriver(UserDTO driver) {
        this.driver = driver;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAllowsSmoking(boolean allowsSmoking) {
        this.allowsSmoking = allowsSmoking;
    }

    public void setAllowsPets(boolean allowsPets) {
        this.allowsPets = allowsPets;
    }

    public void setAllowsLuggage(boolean allowsLuggage) {
        this.allowsLuggage = allowsLuggage;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
