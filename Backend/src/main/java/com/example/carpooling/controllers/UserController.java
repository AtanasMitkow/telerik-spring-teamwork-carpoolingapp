package com.example.carpooling.controllers;

import com.example.carpooling.DTO.*;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.utility.Notification;
import com.example.carpooling.services.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Api(tags = "User")
@SwaggerDefinition(tags = {
        @Tag(name = "User", description = "User operations")
})
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST, RequestMethod.PUT}, allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/users")
public class UserController {
    private ImageService imageService;
    private UserService userService;
    private RatingService ratingService;
    private CarService carService;
    private NotificationService notificationService;

    @Autowired
    public UserController(UserService userService, ImageService imageService, RatingService ratingService,
                          CarService carService,NotificationService notificationService) {
        this.userService = userService;
        this.imageService = imageService;
        this.ratingService = ratingService;
        this.carService = carService;
        this.notificationService = notificationService;
    }

    @ApiOperation(value = "Get user profile by username", response = UserDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @GetMapping("/{username}")
    public UserDTO getByUserName(@PathVariable String username) {
        try {
            return userService.getByUsername(username);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Authenticate as registered user", response = JwtTokenDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 403, message = "You are already logged in"),

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.CREATED)
    public JwtTokenDTO login(@RequestBody LoginDTO loginDetails, HttpServletResponse response) {
        try {
            JwtTokenDTO token = userService.login(loginDetails);
            Cookie cookie = new Cookie("auth", token.getToken());
            cookie.setPath("/");
            cookie.setMaxAge(60 * 60 * 2);
            cookie.setHttpOnly(true);
            cookie.setDomain("localhost");
            response.addCookie(cookie);
            return token;
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Logout")
    @ApiResponses(value = {

            @ApiResponse(code = 403, message = "You are already logged out"),


    })
    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            Cookie cookie = userService.logout(request);
            cookie.setMaxAge(0);
            cookie.setPath("/");
            cookie.setHttpOnly(true);
            cookie.setDomain("localhost");
            //userService.logout(request);
            response.addCookie(cookie);
        } catch (NotLoggedInException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Get currently authenticated user", response = UserDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "Access denied => You are not logged in"),

    })
    @GetMapping("/me")
    public UserDTO getCurrentUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            return userService.getCurrentUser(request);
        } catch (NotLoggedInException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Register as new user", response = UserDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 403, message = "Username already taken")

    })
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO registerUser(@RequestBody CreateUserDTO user) {
        try {
            return userService.registerUser(user);
        } catch (UsernameAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (InvalidPhoneNumberException | NameLengthException | InvalidPasswordException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Update user")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "User updated successfully"),

            @ApiResponse(code = 400, message = "Invalid phone number format"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @PutMapping
    public void updateUser(@RequestBody UserDTO userDTO) {
        try {
            userService.editUser(userDTO);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (InvalidPhoneNumberException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Change user avatar", response = ImageDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Failed to upload image"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @PostMapping("/{userId}/avatar")
    public ImageDTO changeAvatar(@RequestParam("file") MultipartFile file, @PathVariable long userId) {
        try {
            return imageService.uploadUserAvatar(file, userId);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FailedToUploadImageException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @ApiOperation(value = "Get user avatar")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),

            @ApiResponse(code = 404, message = "User/Image does not exist")

    })
    @GetMapping("/{userId}/avatar")
    public void getAvatar(@PathVariable long userId, HttpServletResponse response) {
        try {
            response.setContentType("image/jpeg,image/jpg,image/png");
            response.getOutputStream().write(imageService.getUserAvatar(userId));
            response.getOutputStream().close();
        } catch (UserDoesntExistsException | ImageDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @ApiOperation(value = "Get cars that belong to user", response = Car.class, responseContainer = "List")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to see other users resources"),

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @GetMapping("/{userId}/cars")
    public List<Car> getCars(@PathVariable long userId) {
        try {
            return userService.getOwnedCars(userId);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @ApiOperation(value = "Get drivers LeaderBoard", response = UserDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to get the resource")
    })
    @GetMapping("/drivers")
    public Page<UserDTO> getDriversLeaderBoards(@RequestParam int page,@RequestParam int size) {
        return ratingService.getDriversLeaderBoards(page,size);
    }

    @ApiOperation(value = "Get passengers LeaderBoard", response = UserDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to get the resource")
    })
    @GetMapping("/passengers")
    public Page<UserDTO> getPassengersLeaderBoards(@RequestParam int page ,@RequestParam int size) {
        return ratingService.getPassengersLeaderBoards(page,size);

    }

    @ApiOperation(value = "Get cars that belong to user in page", response = Car.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to see other users resources"),

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @GetMapping("/cars")
    public Page<Car> getCars(@RequestParam int page,@RequestParam int size) {
        try {
            return carService.getAllByOwner(page,size);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @ApiOperation(value = "Get all users", response = UserDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to get the resource")
    })
    @GetMapping
    public Page<UserDTO> getAll(@RequestParam int page,@RequestParam int size)
    {
        return userService.getAll(page,size);
    }

    @ApiOperation(value = "Get notifications", response = NotificationDTO.class, responseContainer = "Page")
    @GetMapping("/notifications")
    public Page<NotificationDTO> getNotifications(@RequestParam int page, @RequestParam int size)
    {
        return notificationService.getNotificationsByUser(page,size);
    }

    @ApiOperation(value = "View notifications")
    @PutMapping("/notifications")
    public void viewNotifications()
    {
        notificationService.viewNotificationsByUser();
    }
}
