package com.example.carpooling.controllers;

import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.TripDoesntExistsException;
import com.example.carpooling.exceptions.UserDoesntExistsException;
import com.example.carpooling.services.AdminService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@Api(tags = {"Admin"})
@SwaggerDefinition(tags = {
        @Tag(name = "Admin functions controller", description = "Admin operations")
})
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.DELETE,RequestMethod.OPTIONS}, allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/admin")
public class AdminController {
    private AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @ApiOperation(value = "Delete a user")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Deleted"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to delete is not found")

    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/users/{userID}")
    public void deleteUser(@PathVariable long userID) {
        try {
            adminService.deleteUser(userID);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a car")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Deleted"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to delete is not found")

    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/cars/{carID}")
    public void deleteCar(@PathVariable long carID) {
        try {
            adminService.deleteCar(carID);
        } catch (CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Delete a trip")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Deleted"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to delete is not found")

    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/trips/{tripID}")
    public void deleteTrip(@PathVariable long tripID) {
        try {
            adminService.deleteTrip(tripID);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
