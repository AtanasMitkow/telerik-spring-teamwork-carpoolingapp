package com.example.carpooling.controllers;

import com.example.carpooling.DTO.*;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.utility.Comment;
import com.example.carpooling.models.utility.Location;
import com.example.carpooling.services.TripService;
import io.swagger.annotations.*;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.List;

@Api(tags = {"Trip"})
@SwaggerDefinition(tags = {
        @Tag(name = "Trip resource controller", description = "Trip operations")
})
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.GET, RequestMethod.PATCH, RequestMethod.OPTIONS, RequestMethod.POST, RequestMethod.PUT}, allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/trips")
public class TripController {

    private TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }


    @ApiOperation(value = "Get trips", response = TripDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 404, message = "User does not exist")

    })
    @GetMapping
    public Page<TripDTO> getTrips(
            @And({
                    @Spec(path = "deleted", spec = Equal.class, constVal = "false"),
                    @Spec(path = "status", params = "status", spec = Equal.class),
                    @Spec(path = "origin.name", params = "origin", spec = Equal.class),
                    @Spec(path = "driver.username", params = "driver", spec = Equal.class),
                    @Spec(path = "destination.name", params = "destination", spec = Equal.class),
                    @Spec(path = "allowsSmoking", params = "allows_smoking", spec = Equal.class),
                    @Spec(path = "allowsPets", params = "allows_pets", spec = Equal.class),
                    @Spec(path = "allowsLuggage", params = "allows_luggage", spec = Equal.class)

            }) Specification<Trip> spec, @RequestParam int page, @RequestParam int size,
            @RequestParam(required = false) String earliest) {

        try {
            return tripService.getAll(spec, page, size, earliest);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Create trip")
    @ApiResponses(value = {

            @ApiResponse(code = 201, message = "Trip created successfully"),

            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),

            @ApiResponse(code = 400, message = "Invalid data provided")

    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody CreateTripDTO trip) throws ParseException {
        try {
            tripService.create(trip);
        } catch (CarDoesntExistsException | UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidDateException | SameOriginDestinationException | TripMessageLengthException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @ApiOperation(value = "Edit trip")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip/Car does not exist")

    })
    @PutMapping
    public void editTrip(@RequestBody EditTripDTO trip) throws ParseException {
        try {
            tripService.update(trip);
        } catch (InvalidDateException | SameOriginDestinationException | TripMessageLengthException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (TripDoesntExistsException | CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Get trip by ID", response = TripDTO.class)
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "Trip does not exist")

    })
    @GetMapping("/{id}")
    public TripDTO getTrip(@PathVariable long id) {
        try {
            return tripService.getTripByID(id);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Change trip status")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip does not exist")

    })
    @PatchMapping("/{id}")
    public void changeTripStatus(@PathVariable long id, @RequestParam String status) {
        try {
            tripService.changeTripStatus(id, status.toUpperCase());
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (ChangeTripStatusException | WrongStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Add comment to trip")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 404, message = "Trip/User does not exist")

    })
    @PostMapping("/{id}/comments")
    public void addComment(@PathVariable long id, @RequestBody CreateCommentDTO comment) {
        try {
            tripService.addComment(id, comment);
        } catch (TripDoesntExistsException | UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CommentLengthException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @ApiOperation(value = "Apply for a trip")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip does not exist"),

            @ApiResponse(code = 409, message = "Already in trip")

    })
    @PostMapping("{id}/passengers")
    public void apply(@PathVariable long id) {
        try {
            tripService.apply(id);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CantApplyForTripWithStatusDifferentFromAvailableException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (AlreadyInTripException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @ApiOperation(value = "Rate a driver base on a trip")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip/User does not exist")

    })
    @PostMapping("/{id}/driver/rate")
    public void rateDriver(@PathVariable long id, @RequestParam double rating) {
        try {
            tripService.rateDriver(id, rating);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CantRateDriverIfTripIsNotDoneException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (CantRateDriverFromTripThatYouAreNotInException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (RatingRangeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @ApiOperation(value = "Rate a passenger based on a trip")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip/User does not exist")

    })
    @PostMapping("/{tripID}/passengers/{passengerID}/rate")
    public void ratePassenger(@PathVariable long tripID, @PathVariable long passengerID, @RequestParam double rating) {
        try {
            tripService.ratePassenger(tripID, passengerID, rating);
        } catch (TripDoesntExistsException | UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CantRateDriverIfTripIsNotDoneException | UserNotFromThisTripException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (RatingRangeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Change passenger status")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),

            @ApiResponse(code = 403, message = "You are not allowed to change other users resources"),

            @ApiResponse(code = 404, message = "Trip/Passenger does not exist")

    })
    @PatchMapping("/{tripID}/passengers/{passengerID}")
    public void changePassengerStatus(@PathVariable long tripID, @PathVariable long passengerID, @RequestParam String status) {
        try {
            tripService.changePassengerStatus(tripID, passengerID, status);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UserDoesntExistsException | PassengerNotInThisTripException | WrongStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (CantChangePassengerStatusIfTripStatusDifferentFromAvailableException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }


    @ApiOperation(value = "Get the passengers of a trip", response = PassengerDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "Trip does not exist")

    })
    @GetMapping("/{tripID}/passengers")
    public Page<PassengerDTO> getTripPassengers(
            @RequestParam(required = false) String status, @PathVariable long tripID, @RequestParam int page, @RequestParam int size) {
        try {
            return tripService.getTripPassengers(status, tripID, page, size);

        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (WrongStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Get the comments of a trip", response = CommentDTO.class, responseContainer = "Page")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "Trip does not exist")

    })
    @GetMapping("/{tripID}/comments")
    public Page<CommentDTO> getTripComments(@PathVariable long tripID, @RequestParam int page, @RequestParam int size) {
        try {
            return tripService.getTripComments(tripID, page, size);

        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Get available locations", response = Location.class, responseContainer = "List")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

    })
    @GetMapping("/locations")
    public List<Location> getAvailableLocations() {
        return tripService.getAvailableLocations();
    }

    @ApiOperation(value = "Check if user is passenger in this trip")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "Trip does not exist")

    })
    @GetMapping("/{tripID}/amIPassenger")
    public StatusDTO amIPassenger(@PathVariable long tripID) {
        try {
            return tripService.amIPassenger(tripID);
        } catch (TripDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
