package com.example.carpooling.controllers;

import com.example.carpooling.DTO.CreateCarDTO;
import com.example.carpooling.DTO.EditCarDTO;
import com.example.carpooling.DTO.ImageDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.services.CarService;
import com.example.carpooling.services.ImageService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Api(tags = {"Car"})
@SwaggerDefinition(tags = {
        @Tag(name = "Car resource controller", description = "Car operations")
})
@CrossOrigin(origins = "*",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PUT, RequestMethod.DELETE}, allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/cars")
public class CarController {

    private CarService carService;
    private ImageService imageService;

    @Autowired
    public CarController(CarService carService, ImageService imageService) {
        this.imageService = imageService;
        this.carService = carService;
    }

    @ApiOperation(value = "Create a car")
    @ApiResponses(value = {

            @ApiResponse(code = 201, message = "Created"),

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody CreateCarDTO carDTO) {
        try {
            carService.create(carDTO);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (CarModelLengthException | CarYearMadeException | CarSeatsNumberException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Edit a car")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Invalid data provided"),

            @ApiResponse(code = 401, message = "You are not authorized to edit the resource"),

            @ApiResponse(code = 404, message = "Car does not exist")

    })
    @PutMapping
    public void edit(@RequestBody EditCarDTO carDTO) {
        try {
            carService.update(carDTO);
        } catch (CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (CarModelLengthException | CarSeatsNumberException | CarYearMadeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Add images to a car")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Car already has too many images"),

            @ApiResponse(code = 401, message = "You are not authorized to edit the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @PostMapping("{carID}/images")
    public void addCarImages(@RequestParam("files") MultipartFile[] files, @PathVariable long carID) {
        try {
            imageService.uploadMultipleFiles(files, carID);
        } catch (FailedToUploadImageException | TooManyCarImagesException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Get a single car image")
    @ApiResponses(value = {

            @ApiResponse(code = 400, message = "Unable to retrieve image"),

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @GetMapping("/{carID}/images/{imageID}")
    public void getCarImage(@PathVariable long carID, @PathVariable long imageID, HttpServletResponse response) {
        try {
            response.setContentType("image/jpeg,image/jpg,image/png");
            response.getOutputStream().write(imageService.getCarImage(carID, imageID));
            response.getOutputStream().close();
        } catch (CarDoesntExistsException | ImageDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Get car images", response = ImageDTO.class, responseContainer = "List")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @GetMapping("/{carID}/images")
    public List<ImageDTO> getCarImages(@PathVariable long carID) {
        try {
            return imageService.getCarImages(carID);
        } catch (CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Delete your car")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @DeleteMapping("/{carID}")
    public void deleteOwnCar(@PathVariable long carID) {
        try {
            carService.deleteOwnCar(carID);
        } catch (CarDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAccessingOwnResources e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Get all cars")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @GetMapping
    public Page<Car> getAllCars(@RequestParam int page, @RequestParam int size) {
        return carService.getAll(page, size);
    }

    @ApiOperation(value = "Get car by ID")
    @ApiResponses(value = {

            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),

            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")

    })
    @GetMapping("/{carID}")
    public Car getCarByID(@PathVariable long carID) {
        try {
            return carService.getCarByID(carID);
        } catch (UserDoesntExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (NotAccessingOwnResources notAccessingOwnResources) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
