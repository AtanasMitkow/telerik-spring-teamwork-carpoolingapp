package com.example.carpooling.services;

import com.example.carpooling.DTO.NotificationDTO;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.models.utility.Notification;
import com.example.carpooling.repositories.NotificationRepository;
import com.example.carpooling.repositories.TripRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.utils.Helper;
import com.example.carpooling.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    private UserRepository userRepository;
    private TripRepository tripRepository;
    private NotificationRepository notificationRepository;
    private Helper helper;

    @Autowired
    public NotificationServiceImpl(UserRepository userRepository,TripRepository tripRepository,Helper helper,NotificationRepository notificationRepository) {
        this.userRepository = userRepository;
        this.tripRepository = tripRepository;
        this.helper = helper;
        this.notificationRepository = notificationRepository;
    }

    @Override
    public void notify(long userID,long tripID, Notification notification) {
        User user = userRepository.findByUserIDAndEnabledTrue(userID);
        Trip trip = tripRepository.findByIdAndDeletedFalse(tripID);
        user.getNotifications().add(notification);
        notification.setUser(user);
        notification.setTrip(trip);
        userRepository.save(user);
    }

    @Override
    public Page<NotificationDTO> getNotificationsByUser(int page, int size) {
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        Pageable pageable = PageRequest.of(page,size, Sort.by("read").ascending());
        Page<Notification> notifications = notificationRepository.getAllByUser(user,pageable);
        Long unreadNotifications = notificationRepository.countByReadFalseAndUser(user);
        return notifications.map(n -> {
            NotificationDTO notificationDTO = new NotificationDTO(n.getId(),n.getMessage(),n.isRead(),n.getDate(),unreadNotifications);
            notificationDTO.setTrip(Mapper.tripToTripDTO(n.getTrip()));
            notificationDTO.setUser(Mapper.userToUserDTO(n.getUser()));
            return notificationDTO;
        });
    }

    @Override
    public void viewNotificationsByUser() {
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        user.getNotifications().forEach(notification -> notification.setRead(true));
        userRepository.save(user);
    }
}
