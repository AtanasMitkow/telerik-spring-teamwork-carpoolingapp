package com.example.carpooling.services;

import com.example.carpooling.DTO.*;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.utility.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.text.ParseException;
import java.util.List;

public interface TripService {

    void create(CreateTripDTO trip) throws ParseException, CarDoesntExistsException, InvalidDateException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException;

    void update(EditTripDTO trip) throws ParseException, TripDoesntExistsException, CarDoesntExistsException, InvalidDateException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException;

    Page<TripDTO> getAll(Specification<Trip> spec, int page, int size, String earliest) throws UserDoesntExistsException;

    Page<PassengerDTO> getTripPassengers(String filter, long tripID, int page, int size) throws WrongStatusException, TripDoesntExistsException;

    TripDTO getTripByID(long id) throws TripDoesntExistsException;

    void changeTripStatus(long id, String status) throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException;

    void addComment(long id, CreateCommentDTO comment) throws TripDoesntExistsException, UserDoesntExistsException, CommentLengthException;

    void apply(long id) throws TripDoesntExistsException, CantApplyForTripWithStatusDifferentFromAvailableException, AlreadyInTripException;

    void rateDriver(long id, double rating) throws TripDoesntExistsException, CantRateDriverIfTripIsNotDoneException, CantRateDriverFromTripThatYouAreNotInException, RatingRangeException;

    void ratePassenger(long tripID, long passengerID, double rating) throws TripDoesntExistsException, CantRateDriverIfTripIsNotDoneException, NotAccessingOwnResources, UserDoesntExistsException, UserNotFromThisTripException, RatingRangeException;

    Page<CommentDTO> getTripComments(long tripID, int page, int size) throws TripDoesntExistsException;

    void changePassengerStatus(long tripID, long passengerID, String status) throws TripDoesntExistsException, UserDoesntExistsException, NotAccessingOwnResources, CantChangePassengerStatusIfTripStatusDifferentFromAvailableException, WrongStatusException, PassengerNotInThisTripException;

    List<Location> getAvailableLocations();

    StatusDTO amIPassenger(long tripID) throws TripDoesntExistsException;
}
