package com.example.carpooling.services;

import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.TripDoesntExistsException;
import com.example.carpooling.exceptions.UserDoesntExistsException;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.TripRepository;
import com.example.carpooling.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    private UserRepository userRepository;
    private CarRepository carRepository;
    private TripRepository tripRepository;

    @Autowired
    public AdminServiceImpl(UserRepository userRepository, CarRepository carRepository, TripRepository tripRepository) {
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.tripRepository = tripRepository;
    }

    @Override
    public void deleteUser(long userID) throws UserDoesntExistsException {
        User user = userRepository.findByUserIDAndEnabledTrue(userID);
        if (user == null) throw new UserDoesntExistsException();
        user.setEnabled(false);
        userRepository.save(user);
    }

    @Override
    public void deleteCar(long carID) throws CarDoesntExistsException {
        Car car = carRepository.findById(carID);
        if (car == null) throw new CarDoesntExistsException();
        car.setDeleted(true);
        carRepository.save(car);
    }

    @Override
    public void deleteTrip(long tripID) throws TripDoesntExistsException {
        Trip trip = tripRepository.findById(tripID);
        if (trip == null) throw new TripDoesntExistsException();
        trip.setDeleted(true);
        tripRepository.save(trip);
    }
}
