package com.example.carpooling.services;

import com.example.carpooling.DTO.CreateCarDTO;
import com.example.carpooling.DTO.EditCarDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import org.springframework.data.domain.Page;


public interface CarService {

    Page<Car> getAll(int page, int size);

    Page<Car> getAllByOwner(int page, int size) throws UserDoesntExistsException, NotAccessingOwnResources;

    void create(CreateCarDTO car) throws UserDoesntExistsException, CarModelLengthException, CarYearMadeException, CarSeatsNumberException;

    void update(EditCarDTO car) throws CarDoesntExistsException, NotAccessingOwnResources, CarModelLengthException, CarYearMadeException, CarSeatsNumberException;

    void deleteOwnCar(long carID) throws CarDoesntExistsException, NotAccessingOwnResources;

    Car getCarByID(long id) throws UserDoesntExistsException, NotAccessingOwnResources;
}
