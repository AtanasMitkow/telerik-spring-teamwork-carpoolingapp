package com.example.carpooling.services;

import com.example.carpooling.DTO.CreateUserDTO;
import com.example.carpooling.DTO.JwtTokenDTO;
import com.example.carpooling.DTO.LoginDTO;
import com.example.carpooling.DTO.UserDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import org.springframework.data.domain.Page;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {

    UserDTO registerUser(CreateUserDTO userToCreate) throws UsernameAlreadyExistsException, InvalidPhoneNumberException, InvalidPasswordException, NameLengthException;

    void editUser(UserDTO userDTO) throws UserDoesntExistsException, NotAccessingOwnResources, InvalidPhoneNumberException;

    UserDTO getByUsername(String username) throws UserDoesntExistsException;

    List<Car> getOwnedCars(long id) throws UserDoesntExistsException, NotAccessingOwnResources;

    JwtTokenDTO login(LoginDTO loginDetails) throws UserDoesntExistsException;

    UserDTO getCurrentUser(HttpServletRequest request) throws NotLoggedInException;

    Cookie logout(HttpServletRequest request) throws NotLoggedInException;

    Cookie getAuthCookie(HttpServletRequest request) throws NotLoggedInException;

    Page<UserDTO> getAll(int page, int size);

}
