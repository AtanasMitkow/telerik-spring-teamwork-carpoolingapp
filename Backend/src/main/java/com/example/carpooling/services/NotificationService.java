package com.example.carpooling.services;

import com.example.carpooling.DTO.NotificationDTO;
import com.example.carpooling.models.User;
import com.example.carpooling.models.utility.Notification;
import org.springframework.data.domain.Page;

public interface NotificationService {

    void notify(long userID,long tripID, Notification notification);

    Page<NotificationDTO> getNotificationsByUser(int page, int size);

    void viewNotificationsByUser();
}
