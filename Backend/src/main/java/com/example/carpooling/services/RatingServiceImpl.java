package com.example.carpooling.services;

import com.example.carpooling.DTO.UserDTO;
import com.example.carpooling.exceptions.RatingRangeException;
import com.example.carpooling.models.Rating;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.repositories.RatingRepository;
import com.example.carpooling.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;

@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository ratingRepository;
    private HashMap<String, Double> avgDriverRatings;
    private HashMap<String, Double> avgPassengerRatings;


    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
        avgDriverRatings = new HashMap<>();
        avgPassengerRatings = new HashMap<>();
    }

    @Override
    public void rateDriver(User rater, User rated, Trip trip, Double rating) throws RatingRangeException {
        Rating userRating = ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip);
        rateAsDriver(userRating, rating, rater, rated, trip);
        updateDriverRating(rated);
    }

    @Override
    public void ratePassenger(User rater, User rated, Trip trip, Double rating) throws RatingRangeException {
        Rating userRating = ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip);
        rateAsPassenger(userRating, rating, rater, rated, trip);
        updatePassengerRating(rated);
    }

    @Override
    public Double getAVGDriverRating(User driver) {
        if (!avgDriverRatings.containsKey(driver.getUsername())) {
            Double avg = ratingRepository.calculateAVGDriverRating(driver);
            avgDriverRatings.put(driver.getUsername(), avg == null ? 0 : avg);
        }
        return formatRating(avgDriverRatings.get(driver.getUsername()));

    }

    @Override
    public Double getAVGPassengerRating(User passenger) {
        if (!avgPassengerRatings.containsKey(passenger.getUsername())) {
            Double rating = ratingRepository.calculateAVGPassengerRating(passenger);
            avgPassengerRatings.put(passenger.getUsername(), rating == null ? 0 : rating);
        }
       return formatRating(avgPassengerRatings.get(passenger.getUsername()));
    }

    @Override
    public Page<UserDTO> getDriversLeaderBoards(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> users = ratingRepository.findByOrderByRatedAsDriver(pageable);
        return convertPageUserToListUserDTO(users);
    }

    @Override
    public Page<UserDTO> getPassengersLeaderBoards(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> users = ratingRepository.findByOrderByRatedAsPassenger(pageable);
        return convertPageUserToListUserDTO(users);
    }

    private void rateAsPassenger(Rating userRating, double rating, User rater, User rated, Trip trip) throws RatingRangeException {
        if (userRating != null) {
            userRating.setRatedAsPassenger(rating);
            userRating.setRatedAsPassengerAt(new Date());
            ratingRepository.save(userRating);
        } else {
            userRating = new Rating(rater, rated, trip, 0, rating, null, new Date());
            ratingRepository.save(userRating);
        }

    }

    private void rateAsDriver(Rating userRating, double rating, User rater, User rated, Trip trip) throws RatingRangeException {
        if (userRating != null) {
            userRating.setRatedAsDriver(rating);
            userRating.setRatedAsDriverAt(new Date());
            ratingRepository.save(userRating);
        } else {
            userRating = new Rating(rater, rated, trip, rating, 0, new Date(), null);
            ratingRepository.save(userRating);
        }

    }

    private void updateDriverRating(User rated) {
        if (avgDriverRatings.containsKey(rated.getUsername())) {
            avgDriverRatings.remove(rated.getUsername());
            getAVGDriverRating(rated);
        }

    }

    private void updatePassengerRating(User rated) {
        if (avgPassengerRatings.containsKey(rated.getUsername())) {
            avgPassengerRatings.remove(rated.getUsername());
            getAVGPassengerRating(rated);
        }
    }

    private Page<UserDTO> convertPageUserToListUserDTO(Page<User> users) {
        return users.map(user -> {
            UserDTO userDTO = Mapper.userToUserDTO(user);
            userDTO.setRatingAsPassenger(getAVGPassengerRating(user));
            userDTO.setRatingAsDriver(getAVGDriverRating(user));
            return userDTO;
        });
    }

    private double formatRating(double rating) {
        NumberFormat format = new DecimalFormat("#0.0");
        String ratingStr = format.format(rating);
        ratingStr = ratingStr.replace(',', '.');
        return Double.parseDouble(ratingStr);
    }


}
