package com.example.carpooling.services;

import com.example.carpooling.DTO.ImageDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;


@Service
public class ImageServiceImpl implements ImageService {

    private UserRepository userRepository;
    private CarRepository carRepository;
    private Helper helper;

    @Autowired
    public ImageServiceImpl(UserRepository userRepository, CarRepository carRepository, Helper helper) {
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.helper = helper;
    }

    @Override
    public ImageDTO uploadUserAvatar(MultipartFile fileToUpload, long id) throws UserDoesntExistsException, FailedToUploadImageException, NotAccessingOwnResources {
        User user = userRepository.findByUserIDAndEnabledTrue(id);
        if (user == null) throw new UserDoesntExistsException();
        if (!helper.attemptsToChangeOwnsResources(user))
            throw new NotAccessingOwnResources("You cannot change other users avatar");
        Image image = new Image();
        try {
            image.setImageData(fileToUpload.getBytes());
            user.getUserDetails().setImage(image);
            user.getUserDetails().setUpdatedAt(new Date());
            userRepository.save(user);
        } catch (IOException e) {
            throw new FailedToUploadImageException();
        }
        return new ImageDTO(user.getUserDetails().getImage().getId(), helper.constructUserAvatarURL(user.getUserID()));

    }

    @Override
    public void uploadMultipleFiles(MultipartFile[] files, long id) throws CarDoesntExistsException, FailedToUploadImageException, NotAccessingOwnResources {
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        Car car = carRepository.findById(id);
        if (car == null) throw new CarDoesntExistsException();
        if (!user.getOwnedCars().contains(car))
            throw new NotAccessingOwnResources("You cannot add images to other users cars");
        Arrays.stream(files).forEach(file -> {
            Image image = new Image();
            try {
                image.setImageData(file.getBytes());
            } catch (IOException e) {
                throw new FailedToUploadImageException();
            }
            if (car.getCarImages().size() == 5) throw new TooManyCarImagesException();
            car.addImage(image);
        });
        car.setUpdatedAt(new Date());
        carRepository.save(car);
    }

    @Override
    public byte[] getUserAvatar(long id) throws UserDoesntExistsException, ImageDoesntExistException {
        User user = userRepository.findByUserIDAndEnabledTrue(id);
        if (user == null) throw new UserDoesntExistsException();
        if (user.getUserDetails().getImage() == null) throw new ImageDoesntExistException();
        return user.getUserDetails().getImage().getImageData();
    }

    @Override
    public byte[] getCarImage(long carID, long pictureID) throws CarDoesntExistsException, ImageDoesntExistException {
        Car car = carRepository.findById(carID);
        if (car == null) throw new CarDoesntExistsException();
        List<Image> carImages = car.getCarImages();
        Image toReturn = carImages.stream().filter(image -> image.getId() == pictureID).findFirst().orElse(null);
        if (toReturn == null) throw new ImageDoesntExistException();
        return toReturn.getImageData();
    }

    @Override
    public List<ImageDTO> getCarImages(long carID) throws CarDoesntExistsException {
        Car car = carRepository.findById(carID);
        if (car == null) throw new CarDoesntExistsException();
        List<Image> carImages = car.getCarImages();
        List<ImageDTO> returnList = new ArrayList<>();
        carImages.forEach(img -> {
            ImageDTO dto = new ImageDTO(img.getId(), helper.constructCarImageURL(carID, img.getId()));
            returnList.add(dto);
        });
        return returnList;
    }
}
