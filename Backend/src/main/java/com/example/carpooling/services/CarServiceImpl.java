package com.example.carpooling.services;

import com.example.carpooling.DTO.CreateCarDTO;
import com.example.carpooling.DTO.EditCarDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.utils.Helper;
import com.example.carpooling.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CarServiceImpl implements CarService {
    private CarRepository carRepository;
    private UserRepository userRepository;
    private Helper helper;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, UserRepository userRepository, Helper helper) {
        this.carRepository = carRepository;
        this.userRepository = userRepository;
        this.helper = helper;
    }

    @Override
    public void create(CreateCarDTO car) throws UserDoesntExistsException, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {
        String username = helper.getLoggedUsername();
        User owner = userRepository.findByUsernameAndEnabledTrue(username);
        if (owner == null) throw new UserDoesntExistsException();
        Car carToCreate = Mapper.createCarDTOToCar(car, owner);
        carToCreate.setCreatedAt(new Date());
        carToCreate.setUpdatedAt(new Date());
        carToCreate.setDeleted(false);
        owner.addOwnedCar(carToCreate);
        userRepository.save(owner);
        carRepository.save(carToCreate);
    }

    @Override
    public void update(EditCarDTO car) throws CarDoesntExistsException, NotAccessingOwnResources, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        Car carToUpdate = carRepository.findById(car.getId());
        if (carToUpdate == null) throw new CarDoesntExistsException();
        if (!user.getOwnedCars().contains(carToUpdate))
            throw new NotAccessingOwnResources("You cannot edit cars that are not yours!");
        carToUpdate = Mapper.editCarDTOToCar(carToUpdate, car);
        carToUpdate.setUpdatedAt(new Date());
        carRepository.save(carToUpdate);
    }

    @Override
    public void deleteOwnCar(long carID) throws CarDoesntExistsException, NotAccessingOwnResources {
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        Car car = carRepository.findById(carID);
        if (car == null) throw new CarDoesntExistsException();
        if (!user.getOwnedCars().contains(car))
            throw new NotAccessingOwnResources("You cannot delete cars that are not yours!");
        car.setDeleted(true);
        car.setUpdatedAt(new Date());
        carRepository.save(car);
    }

    @Override
    public Page<Car> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return carRepository.findAll(pageable);
    }

    public Page<Car> getAllByOwner(int page, int size) throws UserDoesntExistsException, NotAccessingOwnResources {
        User owner = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (owner == null) throw new UserDoesntExistsException();
        if (!helper.attemptsToChangeOwnsResources(owner))
            throw new NotAccessingOwnResources("You cannot see other users cars");
        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
        return carRepository.findByOwnerAndDeletedFalse(pageable, owner);
    }

    @Override
    public Car getCarByID(long id) throws UserDoesntExistsException, NotAccessingOwnResources {
        User owner = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (owner == null) throw new UserDoesntExistsException();
        if (!helper.attemptsToChangeOwnsResources(owner))
            throw new NotAccessingOwnResources("You cannot see other users cars");
        return carRepository.findById(id);
    }

}
