package com.example.carpooling.services;

import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.TripDoesntExistsException;
import com.example.carpooling.exceptions.UserDoesntExistsException;

public interface AdminService {
    void deleteUser(long userID) throws UserDoesntExistsException;

    void deleteCar(long carID) throws CarDoesntExistsException;

    void deleteTrip(long tripID) throws TripDoesntExistsException;
}
