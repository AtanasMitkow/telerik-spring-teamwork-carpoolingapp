package com.example.carpooling.services;

import com.example.carpooling.DTO.UserDTO;
import com.example.carpooling.exceptions.RatingRangeException;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
public interface RatingService {

    void rateDriver(User rater, User rated, Trip trip, Double rating) throws RatingRangeException;

    void ratePassenger(User rater, User rated, Trip trip, Double rating) throws RatingRangeException;

    Double getAVGDriverRating(User driver);

    Double getAVGPassengerRating(User passenger);

    Page<UserDTO> getDriversLeaderBoards(int page, int size);

    Page<UserDTO> getPassengersLeaderBoards(int page, int size);
}
