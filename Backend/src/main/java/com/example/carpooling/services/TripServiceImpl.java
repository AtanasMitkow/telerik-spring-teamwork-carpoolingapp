package com.example.carpooling.services;

import com.example.carpooling.DTO.*;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.models.middletables.composite_ids.TripPassengersID;
import com.example.carpooling.models.middletables.join_tables.TripPassengers;
import com.example.carpooling.models.utility.*;
import com.example.carpooling.repositories.*;
import com.example.carpooling.utils.Helper;
import com.example.carpooling.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

@Service
public class TripServiceImpl implements TripService {
    private static final int PAGE_SIZE = 8;
    private static final int COMMENT_PAGE = 4;
    private TripRepository tripRepository;
    private LocationRepository locationRepository;
    private UserRepository userRepository;
    private CarRepository carRepository;
    private RatingService ratingService;
    private CommentRepository commentRepository;
    private TripPassengersRepository tripPassengersRepository;
    private NotificationService notificationService;
    private Helper helper;


    @Autowired
    public TripServiceImpl(TripRepository tripRepository, LocationRepository locationRepository, UserRepository userRepository,
                           CarRepository carRepository, RatingService ratingService, CommentRepository commentRepository,
                           TripPassengersRepository tripPassengersRepository, Helper helper,NotificationService notificationService) {
        this.tripRepository = tripRepository;
        this.locationRepository = locationRepository;
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.ratingService = ratingService;
        this.tripPassengersRepository = tripPassengersRepository;
        this.commentRepository = commentRepository;
        this.helper = helper;
        this.notificationService = notificationService;
    }

    @Override
    public void create(CreateTripDTO trip) throws ParseException, CarDoesntExistsException, InvalidDateException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException {
        Car car = carRepository.findById(trip.getCarID());
        if (car == null) throw new CarDoesntExistsException();
        User driver = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (driver == null) throw new UserDoesntExistsException();
        if (!driver.getOwnedCars().contains(car))
            throw new NotAccessingOwnResources("This car doesn't belongs to you!");
        Location destination = locationRepository.findByName(trip.getDestination());
        Location origin = locationRepository.findByName(trip.getOrigin());
        if (destination.equals(origin)) throw new SameOriginDestinationException();
        Trip tripToCreate = Mapper.createTripDTOToTrip(trip, driver, car, origin, destination);
        tripToCreate.setCreatedAt(new Date());
        tripToCreate.setEditedAt(new Date());
        tripToCreate.setDeleted(false);
        tripRepository.save(tripToCreate);
    }

    @Override
    public void update(EditTripDTO trip) throws ParseException, TripDoesntExistsException, CarDoesntExistsException, InvalidDateException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException {
        Trip tripToUpdate = getTrip(trip.getId());
        if (!helper.attemptsToChangeOwnsResources(tripToUpdate.getDriver())) {
            throw new NotAccessingOwnResources("You cannot edit trip that is not yours");
        }
        Car car = carRepository.findById(trip.getCarID());
        if (car == null) throw new CarDoesntExistsException();
        if (!tripToUpdate.getDriver().getOwnedCars().contains(car))
            throw new NotAccessingOwnResources("This car doesn't belongs to you!");
        Location destination = locationRepository.findByName(trip.getDestination());
        Location origin = locationRepository.findByName(trip.getOrigin());
        if (destination.equals(origin)) throw new SameOriginDestinationException();
        tripToUpdate = Mapper.editTripDTOToTrip(tripToUpdate, trip, car, origin, destination);
        tripToUpdate.setEditedAt(new Date());
        tripRepository.save(tripToUpdate);
    }

    @Override
    public Page<TripDTO> getAll(Specification<Trip> spec, int page, int size, String earliest) {
        Pageable pageable = PageRequest.of(page, PAGE_SIZE, Sort.by("id").descending());
        if (earliest != null && earliest.equals("true")) {
            pageable = PageRequest.of(page, PAGE_SIZE, Sort.by("TimeOfDeparture").ascending());
        } else if (earliest != null && earliest.equals("false")) {
            pageable = PageRequest.of(page, PAGE_SIZE, Sort.by("TimeOfDeparture").descending());
        }

        Page<Trip> trips = tripRepository.findAll(spec, pageable);

        return trips.map(trip -> {
            TripDTO tripDTO = Mapper.tripToTripDTO(trip);
            tripDTO.getDriver().setRatingAsPassenger(ratingService.getAVGPassengerRating(trip.getDriver()));
            tripDTO.getDriver().setRatingAsDriver(ratingService.getAVGDriverRating(trip.getDriver()));
            return tripDTO;
        });

    }

    @Override
    public TripDTO getTripByID(long id) throws TripDoesntExistsException {
        Trip trip = getTrip(id);
        TripDTO tripDTO = Mapper.tripToTripDTO(trip);
        tripDTO.getDriver().setRatingAsPassenger(ratingService.getAVGPassengerRating(trip.getDriver()));
        tripDTO.getDriver().setRatingAsDriver(ratingService.getAVGDriverRating(trip.getDriver()));
        return tripDTO;
    }

    @Override
    public void changeTripStatus(long id, String status) throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        Trip trip = getTrip(id);

        if (!helper.attemptsToChangeOwnsResources(trip.getDriver()))
            throw new NotAccessingOwnResources("You cannot change the status of a trip you have not created!");
        if (trip.getStatus().equals(TripStatus.DONE) || trip.getStatus().equals(TripStatus.CANCELED))
            throw new ChangeTripStatusException("You cannot change back the status of an ended or canceled trip");
        TripStatus validatedStatus;
        if (status.equals(TripStatus.AVAILABLE.getName()) || status.equals(TripStatus.BOOKED.getName()) || status.equals(TripStatus.CANCELED.getName()) || status.equals(TripStatus.DONE.getName()) || status.equals(TripStatus.ONGOING.getName())) {
            validatedStatus = TripStatus.valueOf(status);
        } else throw new WrongStatusException();
        if (validatedStatus.equals(TripStatus.AVAILABLE))
            throw new ChangeTripStatusException("Cant change status back to available");
        if (validatedStatus.equals(TripStatus.DONE) && !trip.getStatus().equals(TripStatus.ONGOING)) {
            throw new ChangeTripStatusException("You cannot change trip status to Done if previous status is different from Ongoing");
        }
        trip.setStatus(validatedStatus);
        trip.setEditedAt(new Date());
        tripRepository.save(trip);
    }

    @Override
    public void addComment(long id, CreateCommentDTO comment) throws TripDoesntExistsException, UserDoesntExistsException, CommentLengthException {
        Trip trip = getTrip(id);
        User author = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (author == null) throw new UserDoesntExistsException();
        Comment commentToCreate = Mapper.createCommentDTOToComment(comment, author, trip);
        commentToCreate.setCreatedAt(new Date());
        trip.getComments().add(commentToCreate);
        tripRepository.save(trip);
    }

    @Override
    public void apply(long id) throws TripDoesntExistsException, CantApplyForTripWithStatusDifferentFromAvailableException, AlreadyInTripException {
        Trip trip = getTrip(id);

        if (trip.getStatus() != TripStatus.AVAILABLE && trip.getStatus() != TripStatus.BOOKED)
            throw new CantApplyForTripWithStatusDifferentFromAvailableException();
        User user = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (trip.getDriver().equals(user)) throw new AlreadyInTripException();
        TripPassengersID tripPassengersID = new TripPassengersID(id, user.getUserID());
        TripPassengers tripPassengers = tripPassengersRepository.findTripPassengersById(tripPassengersID);
        if (tripPassengers != null && !tripPassengers.getStatus().equals(PassengerStatus.CANCELED) &&
                !tripPassengers.getStatus().equals(PassengerStatus.ABSENT))
            throw new AlreadyInTripException();
        trip.getPassengers().add(user);
        String message = String.format("%s applied for your trip from %s to %s",user.getUsername(),trip.getOrigin().getName(),trip.getDestination().getName());
        notificationService.notify(trip.getDriver().getUserID(),trip.getId(),new Notification(message,new Date()));
        tripRepository.saveAndFlush(trip);
        tripPassengers = tripPassengersRepository.findTripPassengersById(tripPassengersID);
        tripPassengers.setStatus(PassengerStatus.PENDING);
        tripPassengersRepository.save(tripPassengers);
    }

    @Override
    public void rateDriver(long id, double rating) throws TripDoesntExistsException, CantRateDriverIfTripIsNotDoneException, CantRateDriverFromTripThatYouAreNotInException, RatingRangeException {
        Trip trip = getTrip(id);

        if (!trip.getStatus().equals(TripStatus.DONE))
            throw new CantRateDriverIfTripIsNotDoneException();
        User rater = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (!hasRightsToRateDriver(rater, trip))
            throw new CantRateDriverFromTripThatYouAreNotInException();
        User rated = trip.getDriver();
        ratingService.rateDriver(rater, rated, trip, rating);

    }

    @Override
    public void ratePassenger(long tripID, long passengerID, double rating) throws TripDoesntExistsException, CantRateDriverIfTripIsNotDoneException, NotAccessingOwnResources, UserDoesntExistsException, UserNotFromThisTripException, RatingRangeException {
        Trip trip = getTrip(tripID);
        if (!trip.getStatus().equals(TripStatus.DONE))
            throw new CantRateDriverIfTripIsNotDoneException();
        User rater = userRepository.findByUsernameAndEnabledTrue(helper.getLoggedUsername());
        if (!hasRightsToRatePassenger(rater, trip)) {
            throw new NotAccessingOwnResources("Can't rate passengers from foreign trip");
        }
        User rated = userRepository.findByUserIDAndEnabledTrue(passengerID);
        if (rated == null) throw new UserDoesntExistsException();
        if (!trip.getPassengers().contains(rated)) {
            throw new UserNotFromThisTripException();
        }
        ratingService.ratePassenger(rater, rated, trip, rating);
    }

    @Override
    public void changePassengerStatus(long tripID, long passengerID, String status) throws TripDoesntExistsException, UserDoesntExistsException, WrongStatusException, PassengerNotInThisTripException {
        Trip trip = getTrip(tripID);
        User driver = trip.getDriver();
        TripStatus tripStatus = trip.getStatus();

        if (!isValidStatusForTrip(status, tripStatus, driver))
            throw new WrongStatusException();

        User passenger = userRepository.findByUserIDAndEnabledTrue(passengerID);
        if (passenger == null) throw new UserDoesntExistsException();
        if (!trip.getPassengers().contains(passenger))
            throw new PassengerNotInThisTripException();
        TripPassengersID tripPassengersID = new TripPassengersID(tripID, passengerID);
        TripPassengers tripPassengers = tripPassengersRepository.findTripPassengersById(tripPassengersID);
        PassengerStatus passengerStatus = tripPassengers.getStatus();

        if (!isValidStatusForPassenger(status, passengerStatus))
            throw new WrongStatusException();

        tripPassengers.setStatus(PassengerStatus.valueOf(status));
        tripPassengersRepository.save(tripPassengers);
        String message = String.format("Your status in trip from %s to %s has changed to %s",trip.getOrigin().getName(),trip.getDestination().getName(),tripPassengers.getStatus().getName());
        notificationService.notify(tripPassengersID.getUserId(),tripPassengersID.getTripId(),new Notification(message,new Date()));

        if (PassengerStatus.ACCEPTED.getName().equals(status) && isTripFull(trip))
            trip.setStatus(TripStatus.BOOKED);
        else if (!isTripFull(trip) && tripStatus.equals(TripStatus.BOOKED)) trip.setStatus(TripStatus.AVAILABLE);
        trip.setEditedAt(new Date());
        tripRepository.save(trip);
    }

    private boolean isValidStatusForPassenger(String status, PassengerStatus passengerStatus) {
        if (PassengerStatus.ABSENT.getName().equals(status) && !passengerStatus.equals(PassengerStatus.ACCEPTED))
            return false;
        else return !passengerStatus.equals(PassengerStatus.ABSENT) && !passengerStatus.equals(PassengerStatus.CANCELED);
    }

    @Override
    public List<Location> getAvailableLocations() {
        return locationRepository.findAll();
    }

    @Override
    public StatusDTO amIPassenger(long tripID) throws TripDoesntExistsException {
        Trip trip = tripRepository.findById(tripID);
        if (trip == null) throw new TripDoesntExistsException();
        User me = userRepository.findByUsernameLikeAndEnabledTrue(helper.getLoggedUsername());
        TripPassengersID id = new TripPassengersID(trip.getId(), me.getUserID());
        TripPassengers amIPassenger = tripPassengersRepository.findTripPassengersById(id);
        StatusDTO statusDTO = new StatusDTO();
        if (amIPassenger != null && !amIPassenger.getStatus().equals(PassengerStatus.CANCELED)) {
            statusDTO.setAmIPassenger(true);
            statusDTO.setStatus(amIPassenger.getStatus().getName());
        } else {
            statusDTO.setAmIPassenger(false);
            statusDTO.setStatus("no status");
        }
        return statusDTO;
    }

    @Override
    public Page<PassengerDTO> getTripPassengers(String filter, long tripID, int page, int size) throws TripDoesntExistsException {
        Trip trip = getTrip(tripID);
        Pageable pageable = PageRequest.of(page, size);
        Page<TripPassengers> passengers;
        if (filter != null && isValidStatusForPassenger(filter)) {
            passengers = tripPassengersRepository.findPassengersByTripAndStatus(tripID, PassengerStatus.valueOf(filter), pageable);
        } else passengers = tripPassengersRepository.findPassengersByTrip(tripID, pageable);

        return passengers.map(tripPassengers -> {
            User user = userRepository.findByUserIDAndEnabledTrue(tripPassengers.getId().getUserId());
            PassengerDTO passengerDTO = Mapper.userToPassengersDTO(user);
            passengerDTO.setStatus(tripPassengers.getStatus());
            passengerDTO.setRatingAsPassenger(ratingService.getAVGPassengerRating(user));
            return passengerDTO;

        });
    }

    @Override
    public Page<CommentDTO> getTripComments(long tripID, int page, int size) throws TripDoesntExistsException {
        Trip trip = getTrip(tripID);
        Pageable pageable = PageRequest.of(page, COMMENT_PAGE, Sort.by("id").descending());
        Page<Comment> trips = commentRepository.findAllByTrip(trip, pageable);

        return trips.map(comment -> {
            CommentDTO commentDTO = Mapper.commentToCommentDTO(comment);
            commentDTO.getAuthor().setRatingAsDriver(ratingService.getAVGDriverRating(comment.getAuthor()));
            commentDTO.getAuthor().setRatingAsPassenger(ratingService.getAVGPassengerRating(comment.getAuthor()));
            return commentDTO;
        });
    }

    private Trip getTrip(long id) throws TripDoesntExistsException {
        Trip trip = tripRepository.findByIdAndDeletedFalse(id);
        if (trip == null) throw new TripDoesntExistsException();
        return trip;
    }

    private PassengerStatus getPassengerStatus(Trip trip, User user) {
        TripPassengersID id = new TripPassengersID(trip.getId(), user.getUserID());
        TripPassengers tp = tripPassengersRepository.findTripPassengersById(id);
        return tp.getStatus();
    }

    private boolean hasRightsToRateDriver(User rater, Trip trip) {
        if (!trip.getPassengers().contains(rater)) return false;
        TripPassengersID tripPassengersID = new TripPassengersID(trip.getId(), rater.getUserID());
        TripPassengers tripPassengers = tripPassengersRepository.findTripPassengersById(tripPassengersID);
        return tripPassengers.getStatus().equals(PassengerStatus.ACCEPTED);
    }

    private boolean hasRightsToRatePassenger(User rater, Trip trip) {
        return trip.getDriver().equals(rater);
    }

    private boolean isTripFull(Trip trip) {
        List<TripPassengers> tripPassengers = tripPassengersRepository.findPassengersByTripAndStatus(trip.getId(), PassengerStatus.ACCEPTED);
        return tripPassengers.size() == trip.getAvailablePlaces();
    }

    private boolean isValidStatusForTrip(String status, TripStatus tripStatus, User driver) {
        if (PassengerStatus.PENDING.getName().equals(status))
            return false;
        else if (!helper.attemptsToChangeOwnsResources(driver) && !PassengerStatus.CANCELED.getName().equals(status))
            return false;
        else if (PassengerStatus.ACCEPTED.getName().equals(status) && !tripStatus.equals(TripStatus.AVAILABLE))
            return false;
        else if (PassengerStatus.REJECTED.getName().equals(status) && !(tripStatus.equals(TripStatus.AVAILABLE)
                || tripStatus.equals(TripStatus.BOOKED)))
            return false;
        else if (PassengerStatus.CANCELED.getName().equals(status) && !(tripStatus.equals(TripStatus.BOOKED)
                || tripStatus.equals(TripStatus.AVAILABLE)))
            return false;
        else return !PassengerStatus.ABSENT.getName().equals(status) || (tripStatus.equals(TripStatus.DONE)
                    || tripStatus.equals(TripStatus.ONGOING));
    }

    private boolean isValidStatusForPassenger(String status) {
        return status.equals(PassengerStatus.ABSENT.getName()) || status.equals(PassengerStatus.ACCEPTED.getName()) ||
                status.equals(PassengerStatus.CANCELED.getName()) || status.equals(PassengerStatus.PENDING.getName()) ||
                status.equals(PassengerStatus.REJECTED.getName());
    }
}
