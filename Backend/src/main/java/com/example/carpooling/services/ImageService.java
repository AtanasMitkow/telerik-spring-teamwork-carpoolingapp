package com.example.carpooling.services;

import com.example.carpooling.DTO.ImageDTO;
import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.ImageDoesntExistException;
import com.example.carpooling.exceptions.NotAccessingOwnResources;
import com.example.carpooling.exceptions.UserDoesntExistsException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {
    ImageDTO uploadUserAvatar(MultipartFile fileToUpload, long id) throws UserDoesntExistsException, NotAccessingOwnResources;

    void uploadMultipleFiles(MultipartFile[] files, long id) throws CarDoesntExistsException, NotAccessingOwnResources;

    byte[] getUserAvatar(long id) throws UserDoesntExistsException, ImageDoesntExistException;

    byte[] getCarImage(long carID, long pictureID) throws CarDoesntExistsException, ImageDoesntExistException;

    List<ImageDTO> getCarImages(long carID) throws CarDoesntExistsException;
}
