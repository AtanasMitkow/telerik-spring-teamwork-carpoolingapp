package com.example.carpooling.services;

import com.example.carpooling.DTO.*;
import com.example.carpooling.config.jwt_config.JwtTokenProvider;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.JWTBlacklist;
import com.example.carpooling.models.utility.Role;
import com.example.carpooling.repositories.JWTBlacklistRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.utils.Helper;
import com.example.carpooling.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;

@Service
public class UserServiceImpl implements UserService {

    private static final int PAGE_SIZE = 10;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider jwtTokenProvider;
    private AuthenticationManager authenticationManager;
    private JWTBlacklistRepository jwtBlacklistRepository;
    private Helper helper;

    private RatingService ratingService;


    @Autowired
    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider,
                           AuthenticationManager authenticationManager, JWTBlacklistRepository jwtBlacklistRepository,
                           Helper helper, RatingService ratingService) {
        this.userRepository = repository;
        this.passwordEncoder = passwordEncoder;
        this.ratingService = ratingService;
        this.jwtTokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.helper = helper;
        this.jwtBlacklistRepository = jwtBlacklistRepository;

    }

    @Override
    public UserDTO registerUser(CreateUserDTO userToCreate) throws UsernameAlreadyExistsException, InvalidPhoneNumberException, InvalidPasswordException, NameLengthException {
        User user = userRepository.findByUsernameAndEnabledTrue(userToCreate.getUsername());
        if (user != null) throw new UsernameAlreadyExistsException();
        user = new User();
        if (userToCreate.getUsername().length() < 4)
            throw new NameLengthException("Username should be at least 4 symbols!");
        user.setUsername(userToCreate.getUsername());
        if (!helper.isValidPassword(userToCreate.getPassword()))
            throw new InvalidPasswordException(helper.getInvalidPasswordMessage());
        user.setPassword(passwordEncoder.encode(userToCreate.getPassword()));
        UserDetails details = new UserDetails();
        if (userToCreate.getFirstName().length() < 2)
            throw new NameLengthException("First name should be at least 2 symbols!");
        if (userToCreate.getLastName().length() < 2)
            throw new NameLengthException("Last name should be at least 2 symbols!");
        details.setEmail(userToCreate.getEmail());
        details.setFirstName(userToCreate.getFirstName());
        details.setLastName(userToCreate.getLastName());
        if (!helper.isValidPhoneNumber(userToCreate.getPhone())) throw new InvalidPhoneNumberException();
        details.setPhone(userToCreate.getPhone());
        details.setCreatedAt(new Date());
        details.setUpdatedAt(new Date());
        user.setUserDetails(details);
        user.addRole(Role.ROLE_CLIENT);
        user.setEnabled(true);
        userRepository.save(user);
        UserDTO userDTO = Mapper.userToUserDTO(user);
        userDTO.setRatingAsDriver(0);
        userDTO.setRatingAsPassenger(0);
        return userDTO;
    }

    @Override
    public void editUser(UserDTO userDTO) throws UserDoesntExistsException, NotAccessingOwnResources, InvalidPhoneNumberException {
        User userToEdit = userRepository.findByUserIDAndEnabledTrue(userDTO.getId());
        if (userToEdit == null) throw new UserDoesntExistsException();
        if (!helper.attemptsToChangeOwnsResources(userToEdit))
            throw new NotAccessingOwnResources("You cant change other users profile");
        UserDetails details = userToEdit.getUserDetails();
        if (userDTO.getFirstName() != null && userDTO.getFirstName().length() > 1)
            details.setFirstName(userDTO.getFirstName());
        if (userDTO.getLastName() != null && userDTO.getLastName().length() > 1)
            details.setLastName(userDTO.getLastName());
        if (userDTO.getPhone() != null && userDTO.getPhone().length() > 1) {
            if (!helper.isValidPhoneNumber(userDTO.getPhone())) throw new InvalidPhoneNumberException();
            details.setPhone(userDTO.getPhone());
        }
        if (userDTO.getEmail() != null && userDTO.getEmail().length() > 1)
            details.setEmail(userDTO.getEmail());
        details.setUpdatedAt(new Date());
        userToEdit.setUserDetails(details);
        userRepository.save(userToEdit);
    }

    @Override
    public UserDTO getByUsername(String userName) throws UserDoesntExistsException {
        User user = userRepository.findByUsernameAndEnabledTrue(userName);
        if (user == null) throw new UserDoesntExistsException();
        UserDTO userDTO = Mapper.userToUserDTO(user);
        userDTO.setRatingAsDriver(ratingService.getAVGDriverRating(user));
        userDTO.setRatingAsPassenger(ratingService.getAVGPassengerRating(user));
        return userDTO;
    }

    @Override
    public List<Car> getOwnedCars(long id) throws UserDoesntExistsException, NotAccessingOwnResources {
        User user = userRepository.findByUserIDAndEnabledTrue(id);
        if (user == null) throw new UserDoesntExistsException();
        if (!helper.attemptsToChangeOwnsResources(user))
            throw new NotAccessingOwnResources("You cannot see other users cars");

        return new ArrayList<>(user.getOwnedCars());
    }

    @Override
    public JwtTokenDTO login(LoginDTO loginDetails) throws UserDoesntExistsException {
        String username = loginDetails.getUsername();
        if (userRepository.findByUsernameAndEnabledTrue(username) == null) throw new UserDoesntExistsException();
        String password = loginDetails.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        //return new JwtTokenDTO(jwtTokenProvider.createToken(username, userRepository.findByUsernameAndEnabledTrue(username).getRoles()));
        JwtTokenDTO jwtTokenDTO = new JwtTokenDTO();
        jwtTokenDTO.setToken(jwtTokenProvider.createToken(username, userRepository.findByUsernameAndEnabledTrue(username).getRoles()));

        return jwtTokenDTO;
    }

    @Override
    public Cookie logout(HttpServletRequest request) throws NotLoggedInException {
        String token = jwtTokenProvider.resolveToken(request);
        if (token == null) throw new NotLoggedInException();
        JWTBlacklist blacklistedToken = new JWTBlacklist();
        blacklistedToken.setToken(token);
        jwtBlacklistRepository.save(blacklistedToken);
        return getAuthCookie(request);
    }

    @Override
    public UserDTO getCurrentUser(HttpServletRequest req) throws NotLoggedInException {
        String token = jwtTokenProvider.resolveToken(req);
        if (token == null) throw new NotLoggedInException();
        User user = userRepository.findByUsernameAndEnabledTrue(jwtTokenProvider.getUsername(token));
        UserDTO userDTO = Mapper.userToUserDTO(user);
        userDTO.setRatingAsDriver(ratingService.getAVGDriverRating(user));
        userDTO.setRatingAsPassenger(ratingService.getAVGPassengerRating(user));
        return userDTO;
    }

    @Override
    public Cookie getAuthCookie(HttpServletRequest request) throws NotLoggedInException {
        Cookie[] cookies = request.getCookies();
        Cookie cookie = Arrays.stream(cookies).filter(c -> c.getName().equals("auth")).findFirst().orElse(null);
        if (cookie == null) throw new NotLoggedInException();
        return cookie;
    }

    @Override
    public Page<UserDTO> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, PAGE_SIZE, Sort.by("username"));
        Page<User> userPage = userRepository.findAllByEnabledIsTrue(pageable);

        return userPage.map(user -> {
            UserDTO userDTO = Mapper.userToUserDTO(user);
            userDTO.setRatingAsPassenger(ratingService.getAVGPassengerRating(user));
            userDTO.setRatingAsDriver(ratingService.getAVGDriverRating(user));
            return userDTO;
        });
    }


}
