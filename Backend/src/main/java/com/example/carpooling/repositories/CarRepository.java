package com.example.carpooling.repositories;

import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Car findById(long id);

    Page<Car> findByOwnerAndDeletedFalse(Pageable pageable, User owner);

    Page<Car> findAllBy(Pageable pageable);
}
