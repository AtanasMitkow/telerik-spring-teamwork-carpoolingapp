package com.example.carpooling.repositories;

import com.example.carpooling.models.utility.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
