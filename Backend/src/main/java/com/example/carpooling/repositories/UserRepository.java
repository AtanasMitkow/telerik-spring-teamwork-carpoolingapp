package com.example.carpooling.repositories;

import com.example.carpooling.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUserIDAndEnabledTrue(long userID);

    User findByUsernameAndEnabledTrue(String username);

    User findByUsernameLikeAndEnabledTrue(String username);

    Page<User> findAllByEnabledIsTrue(Pageable pageable);

}
