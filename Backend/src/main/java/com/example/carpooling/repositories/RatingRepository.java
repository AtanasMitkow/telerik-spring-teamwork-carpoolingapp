package com.example.carpooling.repositories;

import com.example.carpooling.models.Rating;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    Rating findRatingByRaterAndRatedAndTrip(User rater, User rated, Trip trip);

    @Query("SELECT AVG(r.ratedAsDriver) FROM Rating r WHERE r.rated = ?1 AND r.ratedAsDriver<>0")
    Double calculateAVGDriverRating(User rated);

    @Query("SELECT AVG(r.ratedAsPassenger) FROM Rating r WHERE r.rated = ?1 AND r.ratedAsPassenger<>0")
    Double calculateAVGPassengerRating(User rated);

    @Query(value = "SELECT r.rated FROM Rating r WHERE r.ratedAsDriver<>0 GROUP BY r.rated ORDER BY AVG(r.ratedAsDriver) DESC")
    Page<User> findByOrderByRatedAsDriver(Pageable pageable);

    @Query(value = "SELECT r.rated FROM Rating r WHERE r.ratedAsPassenger<>0 GROUP BY r.rated ORDER BY AVG(r.ratedAsPassenger) DESC")
    Page<User> findByOrderByRatedAsPassenger(Pageable pageable);


}
