package com.example.carpooling.repositories;

import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.models.utility.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification,Long> {
    Page<Notification> getAllByUser(User user, Pageable pageable);
    Long countByReadFalseAndUser(User user);

}
