package com.example.carpooling.repositories;

import com.example.carpooling.models.utility.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    Location findByName(String name);
}
