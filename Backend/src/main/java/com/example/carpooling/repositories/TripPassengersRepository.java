package com.example.carpooling.repositories;

import com.example.carpooling.models.middletables.composite_ids.TripPassengersID;
import com.example.carpooling.models.middletables.join_tables.TripPassengers;
import com.example.carpooling.models.utility.PassengerStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TripPassengersRepository extends JpaRepository<TripPassengers, TripPassengersID> {
    TripPassengers findTripPassengersById(TripPassengersID id);

    @Query("SELECT tp FROM TripPassengers  tp WHERE tp.id.tripId = ?1 AND tp.status = ?2")
    List<TripPassengers> findPassengersByTripAndStatus(long tripId, PassengerStatus status);

    @Query("SELECT tp FROM TripPassengers  tp WHERE tp.id.tripId = ?1")
    Page<TripPassengers> findPassengersByTrip(long tripId,Pageable pageable);

    @Query("SELECT tp FROM TripPassengers  tp WHERE tp.id.tripId = ?1 AND tp.status = ?2")
    Page<TripPassengers> findPassengersByTripAndStatus(long tripId, PassengerStatus status,Pageable pageable);

}
