package com.example.carpooling.repositories;

import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface TripRepository extends JpaRepository<Trip, Long>, JpaSpecificationExecutor<Trip> {

    Trip findById(long id);

    Trip findByIdAndDeletedFalse(long id);

    Page<Trip> findAllByDeletedFalse(Pageable pageable);

    Page<Trip> findAll(Specification spec, Pageable pageable);

    Page<Trip> findAllByDriver(User driver,Pageable pageable);

}
