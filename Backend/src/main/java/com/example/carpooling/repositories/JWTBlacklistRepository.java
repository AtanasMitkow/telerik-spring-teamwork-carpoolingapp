package com.example.carpooling.repositories;

import com.example.carpooling.models.utility.JWTBlacklist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JWTBlacklistRepository extends JpaRepository<JWTBlacklist, Long> {

    JWTBlacklist getByToken(String token);
}
