package com.example.carpooling.utils;

import com.example.carpooling.DTO.*;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Mapper {
    private Helper helper;


    @Autowired
    public Mapper(Helper helper) {
        this.helper = helper;
    }

    public static Trip createTripDTOToTrip(CreateTripDTO tripDTO, User driver, Car car, Location origin, Location destination) throws ParseException, InvalidDateException, TripMessageLengthException {

        Trip trip = new Trip();

        trip.setDriver(driver);
        trip.setCar(car);
        trip.setMessage(tripDTO.getMessage());
        trip.setAllowsLuggage(tripDTO.isAllowsLuggage());
        trip.setAllowsPets(tripDTO.isAllowsPets());
        trip.setAllowsSmoking(tripDTO.isAllowsSmoking());
        trip.setAvailablePlaces(tripDTO.getAvailablePlaces());
        trip.setDestination(destination);
        trip.setOrigin(origin);
        trip.setEstimatedTime(tripDTO.getEstimatedTime());
        trip.setLength(tripDTO.getLength());
        trip.setStatus(TripStatus.AVAILABLE);
        if (!isValidDepartureTime(formatDate(tripDTO.getDepartureTime()))) throw new InvalidDateException();
        trip.setTimeOfDeparture(formatDate(tripDTO.getDepartureTime()));
        return trip;
    }

    public static Trip editTripDTOToTrip(Trip trip, EditTripDTO tripDTO, Car car, Location origin, Location destination) throws ParseException, InvalidDateException, TripMessageLengthException {
        trip.setCar(car);
        if(tripDTO.getDepartureTime() != null && tripDTO.getDepartureTime().length() > 1) {
            if (!isValidDepartureTime(formatDate(tripDTO.getDepartureTime()))) throw new InvalidDateException();
            trip.setTimeOfDeparture(formatDate(tripDTO.getDepartureTime()));
        }
        trip.setOrigin(origin);
        trip.setDestination(destination);
        if(tripDTO.getMessage() !=null && tripDTO.getMessage().length() > 1)
            trip.setMessage(tripDTO.getMessage());
        if(tripDTO.getAvailablePlaces() != 0)
            trip.setAvailablePlaces(tripDTO.getAvailablePlaces());
        trip.setAllowsSmoking(tripDTO.isAllowsSmoking());
        trip.setAllowsPets(tripDTO.isAllowsPets());
        trip.setAllowsLuggage(tripDTO.isAllowsLuggage());
        trip.setLength(tripDTO.getLength());
        trip.setEstimatedTime(tripDTO.getEstimatedTime());
        return trip;
    }

    public static Comment createCommentDTOToComment(CreateCommentDTO commentDTO, User author,Trip trip) throws CommentLengthException {
        Comment comment = new Comment();
        comment.setTrip(trip);
        comment.setMessage(commentDTO.getMessage());
        comment.setAuthor(author);
        return comment;
    }

    public static Car createCarDTOToCar(CreateCarDTO carDTO, User owner) throws CarModelLengthException, CarSeatsNumberException, CarYearMadeException {
        Car car = new Car();
        car.setModel(carDTO.getModel());
        car.setOwner(owner);
        car.setSeats(carDTO.getSeats());
        car.setYearMade(carDTO.getYearMade());
        car.setHasAirConditioner(carDTO.isHasAirCondition());
        return car;
    }

    public static Car editCarDTOToCar(Car car, EditCarDTO carDTO) throws CarModelLengthException, CarYearMadeException, CarSeatsNumberException {
        if(carDTO.getModel() != null && carDTO.getModel().length() > 1)
            car.setModel(carDTO.getModel());
        car.setHasAirConditioner(carDTO.isHasAirConditioner());
        if(carDTO.getYearMade() != 0)
            car.setYearMade(carDTO.getYearMade());
        if(carDTO.getSeats() != 0)
            car.setSeats(carDTO.getSeats());
        return car;
    }

    public static User createUserDTOToUser(User user, CreateUserDTO userDTO) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        UserDetails details = new UserDetails();
        details.setFirstName(userDTO.getFirstName());
        details.setLastName(userDTO.getLastName());
        details.setEmail(userDTO.getEmail());
        details.setPhone(userDTO.getPhone());
        user.setUserDetails(details);
        user.addRole(Role.ROLE_CLIENT);
        user.setEnabled(true);
        return user;
    }

    public static User userDTOToUser(UserDTO userDTO, User user) {
        UserDetails details = user.getUserDetails();
        user.setUsername(userDTO.getUsername());
        details.setFirstName(userDTO.getFirstName());
        details.setLastName(userDTO.getLastName());
        details.setEmail(userDTO.getEmail());
        details.setPhone(userDTO.getPhone());
        user.setUserDetails(details);
        return user;
    }

    public static UserDTO userToUserDTO(User user) {
        UserDTO dto = new UserDTO();
        dto.setUsername(user.getUsername());
        dto.setId(user.getUserID());
        dto.setFirstName(user.getUserDetails().getFirstName());
        dto.setLastName(user.getUserDetails().getLastName());
        dto.setPhone(user.getUserDetails().getPhone());
        dto.setEmail(user.getUserDetails().getEmail());
        Helper helper = new Helper();
        dto.setAvatarUri(helper.constructUserAvatarURL(user.getUserID()));
        return dto;
    }

    public static TripDTO tripToTripDTO(Trip trip) {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setId(trip.getId());
        tripDTO.setDriver(userToUserDTO(trip.getDriver()));
        tripDTO.setCar(trip.getCar());
        tripDTO.setMessage(trip.getMessage());
        tripDTO.setDepartureTime(trip.getTimeOfDeparture());
        tripDTO.setOrigin(trip.getOrigin().getName());
        tripDTO.setDestination(trip.getDestination().getName());
        tripDTO.setAvailablePlaces(trip.getAvailablePlaces());
        tripDTO.setAllowsLuggage(trip.isAllowsLuggage());
        tripDTO.setAllowsPets(trip.isAllowsPets());
        tripDTO.setAllowsSmoking(trip.isAllowsSmoking());
        tripDTO.setStatus(trip.getStatus().getName());
        tripDTO.setEstimatedTime(trip.getEstimatedTime());
        tripDTO.setLength(trip.getLength());
        return tripDTO;
    }

    public static PassengerDTO userToPassengersDTO(User passenger) {
        PassengerDTO passengerDTO = new PassengerDTO();
        passengerDTO.setId(passenger.getUserID());
        passengerDTO.setUsername(passenger.getUsername());
        passengerDTO.setFirstName(passenger.getUserDetails().getFirstName());
        passengerDTO.setLastName(passenger.getUserDetails().getLastName());
        passengerDTO.setEmail(passenger.getUserDetails().getEmail());
        passengerDTO.setPhone(passenger.getUserDetails().getPhone());
        return passengerDTO;

    }

    public static CommentDTO commentToCommentDTO(Comment comment) {
        User author = comment.getAuthor();
        UserDTO authorDTO = userToUserDTO(author);
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setAuthor(authorDTO);
        commentDTO.setId(comment.getId());
        commentDTO.setMessage(comment.getMessage());
        commentDTO.setCreatedAt(comment.getCreatedAt());
        return commentDTO;
    }


    public static Date formatDate(String departureTime) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm");
        return formatter.parse(departureTime);
    }

    private static boolean isValidDepartureTime(Date date) {
        Date currDate = new Date(System.currentTimeMillis());
        return !(date.before(currDate));
    }

}
