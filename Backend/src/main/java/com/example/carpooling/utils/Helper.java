package com.example.carpooling.utils;

import com.example.carpooling.models.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class Helper {
    private static final String BASE_URL = "http://localhost:8080/api";
    private static final String PHONE_NUMBER_PREFIX = "08";
    private static final String PHONE_NUMBER_PREFIX_LONG = "+3598";
    private static final int PHONE_NUMBER_SYMBOLS_COUNT = 10;
    private static final int PHONE_NUMBER_LONG_SYMBOLS_COUNT = 13;
    private static final int MIN_PASSWORD_LENGTH = 8;
    private static final int MAX_PASSWORD_LENGTH = 18;

    public String constructCarImageURL(long carID, long imageID) {
        return BASE_URL + "/cars/" + carID + "/images/" + imageID;
    }

    public String constructUserAvatarURL(long userID) {
        return BASE_URL + "/users/" + userID + "/avatar";
    }

    public String getLoggedUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public boolean attemptsToChangeOwnsResources(User user) {
        return user.getUsername().equals(getLoggedUsername());
    }

    public boolean isValidPhoneNumber(String phoneNumber) {
        if (!phoneNumber.matches("^[0-9+]+$")) return false;
        if (phoneNumber.startsWith(PHONE_NUMBER_PREFIX) && phoneNumber.length() == PHONE_NUMBER_SYMBOLS_COUNT)
            return true;
        if (phoneNumber.startsWith(PHONE_NUMBER_PREFIX_LONG) && phoneNumber.length() == PHONE_NUMBER_LONG_SYMBOLS_COUNT)
            return true;
        return false;
    }

    public boolean isValidPassword(String password) {
        return password.length() >= MIN_PASSWORD_LENGTH && password.length() <= MAX_PASSWORD_LENGTH;
    }

    public String getInvalidPasswordMessage() {
        return String.format("Password must be between %d and %d symbols", MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH);
    }
}
