package com.example.carpooling.exceptions;

public class CommentLengthException extends Throwable {
    private static final String message = "Comment should be between 5 and 50 symbols";

    public CommentLengthException() {
        super(message);
    }
}
