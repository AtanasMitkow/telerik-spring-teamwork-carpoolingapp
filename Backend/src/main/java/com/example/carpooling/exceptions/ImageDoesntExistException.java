package com.example.carpooling.exceptions;

public class ImageDoesntExistException extends Throwable {
    private static final String message = "The image that you are looking for does not exist!";

    public ImageDoesntExistException() {
        super(message);
    }
}
