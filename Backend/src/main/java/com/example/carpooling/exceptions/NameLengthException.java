package com.example.carpooling.exceptions;

public class NameLengthException extends Throwable {
    public NameLengthException(String message) {
        super(message);
    }
}
