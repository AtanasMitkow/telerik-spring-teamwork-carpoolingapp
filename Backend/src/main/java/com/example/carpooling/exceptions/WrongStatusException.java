package com.example.carpooling.exceptions;

public class WrongStatusException extends Throwable {
    private static final String message = "Invalid status";

    public WrongStatusException() {
        super(message);
    }
}
