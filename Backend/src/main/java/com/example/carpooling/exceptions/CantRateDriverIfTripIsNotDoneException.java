package com.example.carpooling.exceptions;

public class CantRateDriverIfTripIsNotDoneException extends Throwable {
    private static final String message = "Can't rate driver if trip is not done";

    public CantRateDriverIfTripIsNotDoneException() {
        super(message);
    }
}
