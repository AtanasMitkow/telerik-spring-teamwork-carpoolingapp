package com.example.carpooling.exceptions;

public class UsernameAlreadyExistsException extends Throwable {
    private static final String message = "Username already in use";

    public UsernameAlreadyExistsException() {
        super(message);
    }
}
