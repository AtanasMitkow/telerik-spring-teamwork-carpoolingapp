package com.example.carpooling.exceptions;

public class AlreadyInTripException extends RuntimeException {
    private static final String message = "You are already a passenger in this trip";

    public AlreadyInTripException() {
        super(message);
    }
}
