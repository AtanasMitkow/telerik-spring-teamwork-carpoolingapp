package com.example.carpooling.exceptions;

public class InvalidPhoneNumberException extends Throwable {
    private static final String message = "Invalid phone number!";

    public InvalidPhoneNumberException() {
        super(message);
    }
}
