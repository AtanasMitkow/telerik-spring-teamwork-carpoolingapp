package com.example.carpooling.exceptions;

public class CarSeatsNumberException extends Throwable {
    private static final String message = "Car should have at least 2 seats";

    public CarSeatsNumberException() {
        super(message);
    }
}
