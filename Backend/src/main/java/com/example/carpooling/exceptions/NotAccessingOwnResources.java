package com.example.carpooling.exceptions;

public class NotAccessingOwnResources extends Throwable {

    public NotAccessingOwnResources(String message) {
        super(message);
    }
}
