package com.example.carpooling.exceptions;

public class CantCommentTripThatYouNotParticipateInException extends Throwable {
    private static final String message = "Can't comment a trip that you didn't participate in";

    public CantCommentTripThatYouNotParticipateInException() {
        super(message);
    }
}
