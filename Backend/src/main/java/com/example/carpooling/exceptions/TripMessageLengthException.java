package com.example.carpooling.exceptions;

public class TripMessageLengthException extends  Throwable{
    private static final String message = "Message should be between 5 and 50 symbols";

    public TripMessageLengthException() {
        super(message);
    }
}
