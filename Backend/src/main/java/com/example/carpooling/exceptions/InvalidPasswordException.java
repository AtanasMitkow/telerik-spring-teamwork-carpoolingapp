package com.example.carpooling.exceptions;

public class InvalidPasswordException extends Throwable {

    public InvalidPasswordException(String message) {
        super(message);
    }
}
