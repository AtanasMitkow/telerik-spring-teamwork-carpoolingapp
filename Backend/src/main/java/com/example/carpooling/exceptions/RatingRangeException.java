package com.example.carpooling.exceptions;

public class RatingRangeException extends Throwable {
    private static final String message = "Rating should be between 1 and 5";

    public RatingRangeException() {
        super(message);
    }
}
