package com.example.carpooling.exceptions;

public class CarModelLengthException extends Throwable {
    private static final String message = "Car model should be more than 5 symbols";

    public CarModelLengthException() {
        super(message);
    }
}
