package com.example.carpooling.exceptions;

public class CantApplyForTripWithStatusDifferentFromAvailableException extends Throwable {
    private static final String message = "Can't apply for trip with status different from Available";

    public CantApplyForTripWithStatusDifferentFromAvailableException() {
        super(message);
    }
}
