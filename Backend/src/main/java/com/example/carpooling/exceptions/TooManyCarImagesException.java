package com.example.carpooling.exceptions;

public class TooManyCarImagesException extends RuntimeException {
    private static final String message = "You cannot add any more images to your car";

    public TooManyCarImagesException() {
        super(message);
    }

}
