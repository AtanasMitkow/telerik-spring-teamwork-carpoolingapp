package com.example.carpooling.exceptions;

public class NotLoggedInException extends Throwable {
    private static final String message = "You are not logged in";

    public NotLoggedInException() {
        super(message);
    }
}
