package com.example.carpooling.exceptions;

public class UserDoesntExistsException extends Throwable {
    private static final String message = "User does not exist!";

    public UserDoesntExistsException() {
        super(message);
    }

}
