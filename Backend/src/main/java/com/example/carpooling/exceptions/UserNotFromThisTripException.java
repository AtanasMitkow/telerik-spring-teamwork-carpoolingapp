package com.example.carpooling.exceptions;

public class UserNotFromThisTripException extends Throwable {
    private static final String message = "Passenger is not from this trip";

    public UserNotFromThisTripException() {
        super(message);
    }
}
