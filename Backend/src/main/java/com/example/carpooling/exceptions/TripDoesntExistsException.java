package com.example.carpooling.exceptions;

public class TripDoesntExistsException extends Throwable {
    private static final String message = "Trip does not exist!";

    public TripDoesntExistsException() {
        super(message);
    }
}
