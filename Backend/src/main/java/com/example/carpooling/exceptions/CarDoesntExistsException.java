package com.example.carpooling.exceptions;

public class CarDoesntExistsException extends Throwable {
    private static final String message = "Car does not exist!";

    public CarDoesntExistsException() {
        super(message);
    }
}
