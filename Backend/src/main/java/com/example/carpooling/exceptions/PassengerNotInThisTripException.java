package com.example.carpooling.exceptions;

public class PassengerNotInThisTripException extends Throwable {
    private static final String message = "Passenger does not exist in the trip";

    public PassengerNotInThisTripException() {
        super(message);
    }
}
