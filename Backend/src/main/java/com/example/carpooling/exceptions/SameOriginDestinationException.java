package com.example.carpooling.exceptions;

public class SameOriginDestinationException extends Throwable{
    private static final String message = "Origin and destination cannot be same";

    public SameOriginDestinationException() {
        super(message);
    }
}
