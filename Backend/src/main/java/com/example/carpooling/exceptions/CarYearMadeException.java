package com.example.carpooling.exceptions;

public class CarYearMadeException extends Throwable {

    private static final String message = "Car year made cannot be bigger than current year";

    public CarYearMadeException() {
        super(message);
    }
}
