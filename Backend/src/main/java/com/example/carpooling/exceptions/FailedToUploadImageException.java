package com.example.carpooling.exceptions;

public class FailedToUploadImageException extends RuntimeException {
    private static final String message = "Something went wrong while uploading the image";

    public FailedToUploadImageException() {
        super(message);
    }
}
