package com.example.carpooling.exceptions;

public class ChangeTripStatusException extends Throwable {
    public ChangeTripStatusException(String message) {
        super(message);
    }
}
