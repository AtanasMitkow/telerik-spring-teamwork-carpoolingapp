package com.example.carpooling.exceptions;

public class InvalidDateException extends Throwable {
    private static final String message = "Invalid date!";

    public InvalidDateException() {
        super(message);
    }
}
