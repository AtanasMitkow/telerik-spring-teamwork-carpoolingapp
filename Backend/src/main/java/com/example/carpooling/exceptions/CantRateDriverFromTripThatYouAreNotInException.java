package com.example.carpooling.exceptions;

public class CantRateDriverFromTripThatYouAreNotInException extends Throwable {
    private static final String message = "You can't rate driver from trip in which you did not participate";

    public CantRateDriverFromTripThatYouAreNotInException() {
        super(message);
    }
}
