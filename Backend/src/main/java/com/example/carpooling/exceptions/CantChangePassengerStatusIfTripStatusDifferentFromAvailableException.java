package com.example.carpooling.exceptions;

public class CantChangePassengerStatusIfTripStatusDifferentFromAvailableException extends Throwable {
    private static final String message = "Can't change passenger status if trip status is different from Available";

    public CantChangePassengerStatusIfTripStatusDifferentFromAvailableException() {
        super(message);
    }
}
