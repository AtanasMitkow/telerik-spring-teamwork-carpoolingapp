package com.example.carpooling.models;

import com.example.carpooling.models.utility.Image;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "user_details")
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_details_id")
    private long id;

    @Column(name = "firstName", nullable = false)
    @Size(min = 2, message = "First name should be at least 2 symbols")
    private String firstName;

    @Column(name = "lastName", nullable = false)
    @Size(min = 2, message = "Last name should be at least 2 symbols")
    private String lastName;

    @Column(name = "email", nullable = false)
    @Size(min = 15, max = 50, message = "Email must be between 15 and 45 symbols!")
    private String email;

    @Column(name = "phone", nullable = false)
    @Size(min = 10, message = "Phone number should be at least 10 numbers")
    private String phone;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "image_id")
    private Image image;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "last_modified")
    @JsonIgnore
    private Date updatedAt;

    public UserDetails() {
    }

    public UserDetails(String firstName, String lastName, String email, String phone,
                       Image image, Date createdAt, Date updatedAt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.image = image;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(firstName == null || firstName.length() < 2)
            throw new IllegalArgumentException("First name must be bigger than 2 symbols");
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if(lastName == null || lastName.length() < 2)
            throw new IllegalArgumentException("Last name must be bigger than 2 symbols");
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email == null || email.length() < 15 || email.length() > 45)
            throw new IllegalArgumentException("Email must be between 15 and 45 symbols!");
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (phone==null || phone.length() < 10) throw new IllegalArgumentException("Phone number should be at least 10 numbers");
        this.phone = phone;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
