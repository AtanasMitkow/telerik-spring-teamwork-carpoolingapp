package com.example.carpooling.models;

import com.example.carpooling.exceptions.RatingRangeException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_ratings")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "rater_id")
    private User rater;

    @ManyToOne
    @JoinColumn(name = "rated_id")
    private User rated;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    private Trip trip;

    @Column(name = "rated_as_driver")
    private double ratedAsDriver = 0;

    @Column(name = "rated_as_passenger")
    private double ratedAsPassenger = 0;

    @JsonIgnore
    @Column(name = "rated_as_passenger_at")
    private Date ratedAsPassengerAt;

    @JsonIgnore
    @Column(name = "rated_as_driver_at")
    private Date ratedAsDriverAt;

    public Rating() {
    }

    public Rating(User rater, User rated, Trip trip, double ratedAsDriver, double ratedAsPassenger,
                  Date ratedAsDriverAt, Date ratedAsPassengerAt) {
        this.rater = rater;
        this.rated = rated;
        this.trip = trip;
        this.ratedAsDriver = ratedAsDriver;
        this.ratedAsPassenger = ratedAsPassenger;
        this.ratedAsDriverAt = ratedAsDriverAt;
        this.ratedAsPassengerAt = ratedAsPassengerAt;
    }

    public Date getRatedAsPassengerAt() {
        return ratedAsPassengerAt;
    }

    public void setRatedAsPassengerAt(Date ratedAsPassengerAt) {
        this.ratedAsPassengerAt = ratedAsPassengerAt;
    }

    public Date getRatedAsDriverAt() {
        return ratedAsDriverAt;
    }

    public void setRatedAsDriverAt(Date ratedAsDriverAt) {
        this.ratedAsDriverAt = ratedAsDriverAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getRater() {
        return rater;
    }

    public void setRater(User rater) {
        this.rater = rater;
    }

    public User getRated() {
        return rated;
    }

    public void setRated(User rated) {
        this.rated = rated;
    }

    public double getRatedAsDriver() {
        return ratedAsDriver;
    }

    public void setRatedAsDriver(double ratedAsDriver) throws RatingRangeException {
        if (ratedAsDriver < 0 || ratedAsDriver > 5)
            throw new RatingRangeException();

        this.ratedAsDriver = ratedAsDriver;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public double getRatedAsPassenger() {
        return ratedAsPassenger;
    }

    public void setRatedAsPassenger(double ratedAsPassenger) throws RatingRangeException {
        if (ratedAsPassenger < 1 || ratedAsPassenger > 5)
            throw new RatingRangeException();
        this.ratedAsPassenger = ratedAsPassenger;
    }
}
