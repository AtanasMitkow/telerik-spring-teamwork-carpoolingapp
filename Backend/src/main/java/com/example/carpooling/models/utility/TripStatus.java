package com.example.carpooling.models.utility;


public enum TripStatus {
    AVAILABLE("AVAILABLE"), BOOKED("BOOKED"), ONGOING("ONGOING"), DONE("DONE"), CANCELED("CANCELED");
    private String name;

    TripStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
