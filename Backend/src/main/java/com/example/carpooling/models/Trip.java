package com.example.carpooling.models;

import com.example.carpooling.exceptions.TripMessageLengthException;
import com.example.carpooling.models.utility.Comment;
import com.example.carpooling.models.utility.Location;
import com.example.carpooling.models.utility.TripStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CollectionId;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "trips")
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_id")
    private long id;

    @Column(name = "message")
    @Size(min = 5, max = 150, message = "Message should be between 5 and 150 symbols")
    private String message;

    @Column(name = "places", nullable = false)
    private int availablePlaces;

    @Column(name = "departure", nullable = false)
    private Date timeOfDeparture;

    @JsonIgnore
    @Column(name = "last_modified")
    private Date editedAt;

    @Column(name = "is_deleted")
    private boolean deleted;

    @Column(name = "created")
    private Date createdAt;

    @Column(name = "status")
    private TripStatus status;

    @Column(name = "allows_smoking")
    private boolean allowsSmoking;

    @Column(name = "allows_pets")
    private boolean allowsPets;

    @Column(name = "allows_luggage")
    private boolean allowsLuggage;

    @Column(name = "length")
    private int length;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User driver;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "trip_passengers",
            joinColumns = @JoinColumn(name = "trip_id")
            , inverseJoinColumns = @JoinColumn(name = "user_id"))
    @JsonIgnore
    private Set<User> passengers;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    private Car car;

    @OneToMany(mappedBy = "trip",cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonIgnore
    private List<Comment> comments;

    @ManyToOne
    @JoinColumn(name = "origin_id")
    private Location origin;

    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Location destination;

    @Column(name = "estimated_time")
    private int estimatedTime;


    public Trip() {
    }

    public Trip(String message, int availablePlaces, Date timeOfDeparture, TripStatus status,
                boolean allowsSmoking, boolean allowsPets, boolean allowsLuggage, int length,
                boolean isDeleted, User driver, Car car, Location origin, Location destination,
                Date createdAt, Date editedAt, int estimatedTime) {
        this.message = message;
        this.availablePlaces = availablePlaces;
        this.timeOfDeparture = timeOfDeparture;
        this.status = status;
        this.deleted = isDeleted;
        this.allowsSmoking = allowsSmoking;
        this.allowsPets = allowsPets;
        this.allowsLuggage = allowsLuggage;
        this.length = length;
        this.driver = driver;
        passengers = new HashSet<>();
        this.car = car;
        comments = new ArrayList<>();
        this.origin = origin;
        this.destination = destination;
        this.createdAt = createdAt;
        this.editedAt = editedAt;
        this.estimatedTime = estimatedTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getEditedAt() {
        return editedAt;
    }

    public void setEditedAt(Date editedAt) {
        this.editedAt = editedAt;
    }

    public void addPassenger(User user) {
        this.passengers.add(user);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) throws TripMessageLengthException {
        if (message.length() < 5 || message.length() > 150)
            throw new TripMessageLengthException();
        this.message = message;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        if (availablePlaces < 1 || availablePlaces > 8)
            throw new IllegalArgumentException("Available places should be between 1 and 8");
        this.availablePlaces = availablePlaces;
    }

    public Date getTimeOfDeparture() {
        return timeOfDeparture;
    }

    public void setTimeOfDeparture(Date timeOfDeparture) {
        this.timeOfDeparture = timeOfDeparture;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public boolean isAllowsSmoking() {
        return allowsSmoking;
    }

    public void setAllowsSmoking(boolean allowsSmoking) {
        this.allowsSmoking = allowsSmoking;
    }

    public boolean isAllowsPets() {
        return allowsPets;
    }

    public void setAllowsPets(boolean allowsPets) {
        this.allowsPets = allowsPets;
    }

    public boolean isAllowsLuggage() {
        return allowsLuggage;
    }

    public void setAllowsLuggage(boolean allowsLuggage) {
        this.allowsLuggage = allowsLuggage;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public Set<User> getPassengers() {
        return passengers;
    }

    public void setPassengers(Set<User> passengers) {
        this.passengers = passengers;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Location getOrigin() {
        return origin;
    }

    public void setOrigin(Location origin) {
        this.origin = origin;
    }

    public Location getDestination() {
        return destination;
    }

    public void setDestination(Location destination) {
        this.destination = destination;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }
}
