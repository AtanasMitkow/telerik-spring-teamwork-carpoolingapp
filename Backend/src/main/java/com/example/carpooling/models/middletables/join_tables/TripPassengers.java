package com.example.carpooling.models.middletables.join_tables;

import com.example.carpooling.models.middletables.composite_ids.TripPassengersID;
import com.example.carpooling.models.utility.PassengerStatus;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "trip_passengers")
public class TripPassengers {
    @EmbeddedId
    private TripPassengersID id;

    @Column(name = "status")
    private PassengerStatus status;

    public TripPassengers() {
    }

    public TripPassengers(TripPassengersID id, PassengerStatus status) {
        this.id = id;
        this.status = status;
    }

    public PassengerStatus getStatus() {
        return status;
    }

    public void setStatus(PassengerStatus status) {
        this.status = status;
    }

    public TripPassengersID getId() {
        return id;
    }

    public void setId(TripPassengersID id) {
        this.id = id;
    }

}
