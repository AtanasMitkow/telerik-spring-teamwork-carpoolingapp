package com.example.carpooling.models;

import com.example.carpooling.exceptions.CarModelLengthException;
import com.example.carpooling.exceptions.CarSeatsNumberException;
import com.example.carpooling.exceptions.CarYearMadeException;
import com.example.carpooling.models.utility.Image;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "model", nullable = false)
    @Size(min = 6, message = "Car model should be more than 5 symbols")
    private String model;

    @Column(name = "seats", nullable = false)
    private int seats;

    @Column(name = "air_conditioner")
    private boolean hasAirConditioner;

    @Column(name = "year_made", nullable = false)
    private int yearMade;

    @Column(name = "is_deleted")
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User owner;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    private List<Image> carImages;

    @Column(name = "created_at")
    private Date createdAt;

    @JsonIgnore
    @Column(name = "last_updated")
    private Date updatedAt;

    public Car() {
        carImages = new ArrayList<>();
    }

    public Car(String model, int seats, boolean hasAirConditioner, boolean isDeleted, int yearMade, User owner,
               Date createdAt, Date updatedAt) {
        this.model = model;
        this.seats = seats;
        this.hasAirConditioner = hasAirConditioner;
        this.yearMade = yearMade;
        this.owner = owner;
        this.deleted = isDeleted;
        carImages = new ArrayList<>();
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isHasAirConditioner() {
        return hasAirConditioner;
    }

    public int getYearMade() {
        return yearMade;
    }

    public User getOwner() {
        return owner;
    }

    public List<Image> getCarImages() {
        return carImages;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setModel(String model) throws CarModelLengthException {
        if (model.length() < 6) throw new CarModelLengthException();
        this.model = model;
    }

    public void setSeats(int seats) throws CarSeatsNumberException {
        if (seats < 2) throw new CarSeatsNumberException();
        this.seats = seats;
    }

    public void setHasAirConditioner(boolean hasAirConditioner) {
        this.hasAirConditioner = hasAirConditioner;
    }

    public void setYearMade(int yearMade) throws CarYearMadeException {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (yearMade > year) throw new CarYearMadeException();
        this.yearMade = yearMade;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setCarImages(List<Image> carImages) {
        this.carImages = carImages;
    }

    public void addImage(Image image) {
        this.carImages.add(image);
    }
}
