package com.example.carpooling.models.utility;

import javax.persistence.*;

@Entity
@Table(name = "blacklist")
public class JWTBlacklist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id", nullable = false)
    private long id;

    @Column(name = "token", nullable = false)
    private String token;

    public JWTBlacklist() {
    }

    public JWTBlacklist(String token) {
        this.token = token;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
