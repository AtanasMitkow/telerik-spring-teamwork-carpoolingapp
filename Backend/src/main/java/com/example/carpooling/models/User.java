package com.example.carpooling.models;

import com.example.carpooling.models.utility.Comment;
import com.example.carpooling.models.utility.Notification;
import com.example.carpooling.models.utility.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long userID;

    @Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
    @Column(name = "username", unique = true)
    private String username;

    @Size(min = 8, message = "Minimum password length: 8 characters")
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "roles",
            joinColumns = @JoinColumn(name = "username")
    )
    private List<Role> roles = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_details_id")
    private UserDetails userDetails;

    @ManyToMany(mappedBy = "passengers")
    private List<Trip> signedTrips;

    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    private Set<Car> ownedCars = new HashSet<>();

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonIgnore
    private List<Notification> notifications = new ArrayList<>();


    public User() {

    }

    public User(String username, String password, boolean enabled, UserDetails userDetails) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.userDetails = userDetails;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void addOwnedCar(Car car) {
        this.ownedCars.add(car);
    }

    public long getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public List<Trip> getSignedTrips() {
        return signedTrips;
    }

    public Set<Car> getOwnedCars() {
        return ownedCars;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public void setUsername(String username) {
        if (username.length() < 4 || username.length() > 255)
            throw new IllegalArgumentException("Minimum username length: 4 characters");
        this.username = username;
    }

    public void setPassword(String password) {
        if (password.length() < 8) throw new IllegalArgumentException("Minimum password length: 8 characters");
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public void setSignedTrips(List<Trip> signedTrips) {
        this.signedTrips = signedTrips;
    }

    public void setOwnedCars(Set<Car> ownedCars) {
        this.ownedCars = ownedCars;
    }
}
