package com.example.carpooling.models.utility;

import com.example.carpooling.exceptions.CommentLengthException;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "message", nullable = false)
    @Size(min = 5, max = 150, message = "Comment should be between 5 and 150 symbols")
    private String message;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne
    @JoinColumn(name="trip_id")
    private Trip trip;



    public Comment() {
    }

    public Comment(String message, User author, Date createdAt, boolean isDeleted) {
        this.message = message;
        this.author = author;
        this.createdAt = createdAt;
        this.isDeleted = isDeleted;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public void setMessage(String message) throws CommentLengthException {
        if (message.length() < 5 || message.length() > 150)
            throw new CommentLengthException();
        this.message = message;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getAuthor() {
        return author;
    }
}
