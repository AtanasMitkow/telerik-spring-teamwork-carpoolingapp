package com.example.carpooling.models.middletables.composite_ids;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TripPassengersID implements Serializable {
    @Column(name = "trip_id")
    private long tripId;

    @Column(name = "user_id")
    private long userId;

    public TripPassengersID(long tripId, long userId) {
        this.tripId = tripId;
        this.userId = userId;
    }

    public TripPassengersID() {
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TripPassengersID)) return false;
        TripPassengersID that = (TripPassengersID) o;
        return getTripId() == that.getTripId() &&
                getUserId() == that.getUserId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTripId(), getUserId());
    }
}