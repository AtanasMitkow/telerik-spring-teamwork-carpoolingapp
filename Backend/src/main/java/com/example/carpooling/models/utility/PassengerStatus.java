package com.example.carpooling.models.utility;

public enum PassengerStatus {
    PENDING("PENDING"), ACCEPTED("ACCEPTED"), REJECTED("REJECTED"), CANCELED("CANCELED"), ABSENT("ABSENT");
    private String name;

    PassengerStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
