package com.example.carpooling.mockobjects;

import com.example.carpooling.DTO.CreateCommentDTO;
import com.example.carpooling.DTO.CreateTripDTO;
import com.example.carpooling.DTO.EditTripDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.*;
import com.example.carpooling.models.middletables.composite_ids.TripPassengersID;
import com.example.carpooling.models.middletables.join_tables.TripPassengers;
import com.example.carpooling.models.utility.*;
import com.example.carpooling.repositories.*;
import com.example.carpooling.services.RatingService;
import com.example.carpooling.services.TripServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TripServiceImplTest {

    @Mock
    private TripRepository tripRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CarRepository carRepository;

    @Mock
    private LocationRepository locationRepository;

    @Mock
    private User driver;

    @Mock
    private TripPassengersRepository tripPassengersRepository;

    @Mock
    private RatingService ratingService;

    @Mock
    private CommentRepository commentRepository;

    private TripPassengersID tripPassengersID;
    private CreateCommentDTO commentDTO;
    private Comment comment;
    private Car car;
    private Trip trip;
    private Date date;
    private TripStatus tripStatus;
    private EditTripDTO editTripDTO;
    private UserDetails userDetails;
    private Image image;
    private Location origin;
    private Location destination;
    private CreateTripDTO tripDTO;
    private String date1 = "31-12-19";
    private Set<Car> cars = new HashSet<>();
    private TripPassengers tripPassengers;
    private User user;
    private Rating rating;
    private Car car2;

    @Mock
    private com.example.carpooling.utils.Helper helper;

    @Spy
    @InjectMocks
    private TripServiceImpl tripService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.car2 = new Car("golf 5", 4, true, false, 2007, driver, date, date);
        this.car = new Car("golf 4", 4, true, false, 2007, driver, date, date);
        String departureTime = "31-12-19 11:30";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy HH:mm");
        this.userDetails = new UserDetails("pesho", "petrov", "email@abv.bg", "0888888888", image, date, date);
        this.rating = new Rating();
        this.commentDTO = new CreateCommentDTO("message");
        this.comment = new Comment("message", user, date, false);
        this.user = new User("passenger", "password", true, userDetails);
        this.tripPassengersID = new TripPassengersID(1, 2);
        this.tripPassengers = new TripPassengers(tripPassengersID, PassengerStatus.PENDING);
        this.editTripDTO = new EditTripDTO(1, 1, "message", "31-12-19 11:30", "Yambol", "Sofia", 4, true, true, true, 10, 10);
        this.tripStatus = TripStatus.AVAILABLE;
        this.image = new Image();
        this.origin = new Location("Yambol");
        this.destination = new Location("Sofia");
        this.driver = new User("driver", "password", true, userDetails);
        this.driver.addOwnedCar(car);
        this.trip = new Trip("message", 4, date, TripStatus.AVAILABLE, true, true, true, 200, false, driver, car, origin, destination, date, date, 10);
        this.tripDTO = new CreateTripDTO(1, "message", "31-12-19 11:30", "Yambol", "Sofia", 4, true, true, true, 10, 10);
        this.cars.add(this.car);
    }

    @Test
    public void createShouldCreate() throws InvalidDateException, CarDoesntExistsException,
            ParseException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException {
        when(helper.getLoggedUsername()).thenReturn("name");
        driver.getOwnedCars().add(car);
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(driver);
        when(carRepository.findById(1)).thenReturn(car);
        when(locationRepository.findByName("Yambol")).thenReturn(origin);
        when(locationRepository.findByName("Sofia")).thenReturn(destination);
        tripService.create(tripDTO);
        verify(tripService, Mockito.times(1)).create(tripDTO);
    }

    @Test(expected = CarDoesntExistsException.class)
    public void createShouldThrowExceptionWhenCarDoesntExists() throws InvalidDateException, CarDoesntExistsException,
            ParseException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException {
        when(carRepository.findById(1)).thenReturn(null);
        tripService.create(tripDTO);
    }

    @Test(expected = UserDoesntExistsException.class)
    public void createShouldThrowExceptionWhenUserDoesntExists() throws InvalidDateException, CarDoesntExistsException,
            ParseException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException {
        when(carRepository.findById(1)).thenReturn(car);
        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(null);
        tripService.create(tripDTO);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void createShouldThrowExceptionWhenNotAccessingOwnResources() throws InvalidDateException, CarDoesntExistsException,
            ParseException, NotAccessingOwnResources, UserDoesntExistsException, SameOriginDestinationException, TripMessageLengthException {
        when(carRepository.findById(1)).thenReturn(car);
        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(driver);
        driver.getOwnedCars().remove(car);
        tripService.create(tripDTO);
    }

    @Test
    public void updateShouldUpdate() throws InvalidDateException, CarDoesntExistsException, TripDoesntExistsException, ParseException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException {
        driver.getOwnedCars().add(car);
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(carRepository.findById(1)).thenReturn(car);
        when(locationRepository.findByName("Yambol")).thenReturn(origin);
        when(locationRepository.findByName("Sofia")).thenReturn(destination);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        tripService.update(editTripDTO);
        verify(tripService, times(1)).update(editTripDTO);

    }

    @Test(expected = CarDoesntExistsException.class)
    public void updateShouldThrowExceptionWhenCarDoesntExists() throws InvalidDateException, CarDoesntExistsException, TripDoesntExistsException, ParseException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(carRepository.findById(1)).thenReturn(null);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        tripService.update(editTripDTO);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void updateShouldThrowExceptionWhenNotAccessingOwnResources() throws InvalidDateException, CarDoesntExistsException, TripDoesntExistsException, ParseException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(false);
        tripService.update(editTripDTO);
    }

    @Test(expected = TripDoesntExistsException.class)
    public void updateShouldThrowExceptionWhenTripDoesntExists() throws InvalidDateException, CarDoesntExistsException, TripDoesntExistsException, ParseException, NotAccessingOwnResources, SameOriginDestinationException, TripMessageLengthException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(null);
        tripService.update(editTripDTO);
    }

    @Test
    public void getAllShouldReturnTrips() {
        Pageable pageable = PageRequest.of(0, 8, Sort.by("id").descending());
        Specification spec = mock(Specification.class);
        ArrayList<Trip> trips = new ArrayList<>(Collections.singletonList(trip));
        when(tripRepository.findAll(spec, pageable)).thenReturn(new PageImpl<>(trips));
        when(ratingService.getAVGDriverRating(driver)).thenReturn(2.0);
        when(ratingService.getAVGPassengerRating(driver)).thenReturn(3.0);
        tripService.getAll(spec, 0, 8, null);
        Assert.assertEquals(1, tripService.getAll(spec, 0, 8, null).getTotalElements());
    }

    @Test
    public void getTripByIDShouldReturnTrip() throws TripDoesntExistsException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        tripService.getTripByID(1);
        verify(tripRepository, times(1)).findByIdAndDeletedFalse(1);
    }

    @Test(expected = TripDoesntExistsException.class)
    public void getTripByIDShouldThrowExceptionWhenTripDoesntExists() throws TripDoesntExistsException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(null);
        tripService.getTripByID(1);
    }

    @Test
    public void changeTripStatusShouldChange() throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        tripService.changeTripStatus(1, "BOOKED");
        Assert.assertEquals("BOOKED", trip.getStatus().getName());
    }

    @Test(expected = TripDoesntExistsException.class)
    public void changeTripStatusShouldThrowExceptionWhenTripDoesntExists() throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(null);
        tripService.changeTripStatus(1, "BOOKED");
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void changeTripStatusShouldThrowExceptionWhenNotAccessingOwnResources() throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(false);
        tripService.changeTripStatus(1, "BOOKED");
    }

    @Test(expected =ChangeTripStatusException.class)
    public void changeTripStatusShouldThrowExceptionWhenTripStatusIsDone() throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        trip.setStatus(TripStatus.DONE);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        tripService.changeTripStatus(1, "BOOKED");
    }

    @Test(expected = WrongStatusException.class)
    public void changeTripStatusShouldThrowExceptionWhenTripStatusIsWrong() throws TripDoesntExistsException, NotAccessingOwnResources, ChangeTripStatusException, WrongStatusException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        tripService.changeTripStatus(1, "BOOKER");
    }

    @Test
    public void addCommentShouldAddComment() throws TripDoesntExistsException, UserDoesntExistsException, CommentLengthException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        when(helper.getLoggedUsername()).thenReturn("passenger");
        trip.setId(1);
        user.setUserID(2);
        trip.getPassengers().add(user);
        trip.setStatus(TripStatus.DONE);
        when(userRepository.findByUsernameAndEnabledTrue("passenger")).thenReturn(user);
        tripService.addComment(1, commentDTO);
        verify(tripService, times(1)).addComment(1, commentDTO);
    }

    @Test
    public void applyShouldApply() throws TripDoesntExistsException, CantApplyForTripWithStatusDifferentFromAvailableException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        trip.setStatus(TripStatus.AVAILABLE);
        trip.setDriver(driver);
        trip.setId(1);
        user.setUserID(2);
        tripPassengers.setStatus(PassengerStatus.ABSENT);
        when(helper.getLoggedUsername()).thenReturn("passenger");
        when(userRepository.findByUsernameAndEnabledTrue("passenger")).thenReturn(user);
        when(tripPassengersRepository.findTripPassengersById(tripPassengersID)).thenReturn(tripPassengers);
        tripService.apply(1);
        verify(tripService, times(1)).apply(1);
    }

    @Test
    public void rateDriverShouldRate() throws CantRateDriverIfTripIsNotDoneException, TripDoesntExistsException,
            CantRateDriverFromTripThatYouAreNotInException, RatingRangeException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        trip.setStatus(TripStatus.DONE);
        trip.getPassengers().add(user);
        user.setUserID(2);
        trip.setId(1);
        tripPassengersID.setTripId(1);
        tripPassengersID.setUserId(2);
        when(tripPassengersRepository.findTripPassengersById(tripPassengersID)).thenReturn(tripPassengers);
        tripPassengers.getId().setUserId(2);
        tripPassengers.getId().setTripId(1);
        tripPassengers.setStatus(PassengerStatus.ACCEPTED);
        when(helper.getLoggedUsername()).thenReturn("passenger");
        when(userRepository.findByUsernameAndEnabledTrue("passenger")).thenReturn(user);
        doNothing().when(ratingService).rateDriver(user, driver, trip, 4.1);

        tripService.rateDriver(1, 4.1);
        verify(tripService, times(1)).rateDriver(1, 4.1);
    }

    @Test
    public void changePassengerStatus() throws UserDoesntExistsException,
            TripDoesntExistsException, WrongStatusException, PassengerNotInThisTripException {
        when(helper.attemptsToChangeOwnsResources(driver)).thenReturn(true);
        when(userRepository.findByUserIDAndEnabledTrue(2)).thenReturn(user);
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        trip.getPassengers().add(user);
        trip.setStatus(TripStatus.AVAILABLE);
        user.setUserID(2);
        trip.setId(1);
        tripPassengersID.setTripId(1);
        tripPassengersID.setUserId(2);
        when(tripPassengersRepository.findTripPassengersById(tripPassengersID)).thenReturn(tripPassengers);
        tripPassengers.getId().setUserId(2);
        tripPassengers.setStatus(PassengerStatus.PENDING);
        tripPassengers.getId().setTripId(1);
        tripService.changePassengerStatus(1, 2, "ACCEPTED");
        verify(tripService, times(1)).changePassengerStatus(1, 2, "ACCEPTED");

    }

    @Test
    public void getTripPassengersShouldReturnPassengers() throws TripDoesntExistsException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        trip.setId(1);
        user.setUserID(2);
        trip.getPassengers().add(user);
        tripPassengersID.setTripId(1);
        tripPassengersID.setUserId(2);
        when(userRepository.findByUserIDAndEnabledTrue(2)).thenReturn(user);
        Pageable pageable = PageRequest.of(0, 1);
        when(tripPassengersRepository.findPassengersByTripAndStatus(1, PassengerStatus.ACCEPTED, pageable)).thenReturn(new PageImpl<>(Collections.singletonList(tripPassengers)));
        tripPassengers.getId().setUserId(2);
        tripPassengers.setStatus(PassengerStatus.PENDING);
        tripPassengers.getId().setTripId(1);
        String filter = "ACCEPTED";
        tripService.getTripPassengers(filter, 1, 0, 1);
        verify(tripService, times(1)).getTripPassengers(filter, 1, 0, 1);
    }

    @Test
    public void getTripCommentsShouldReturnComments() throws TripDoesntExistsException {
        when(tripRepository.findByIdAndDeletedFalse(1)).thenReturn(trip);
        Pageable pageable = PageRequest.of(0, 4, Sort.by("id").descending());
        when(commentRepository.findAllByTrip(trip, pageable)).thenReturn(new PageImpl<>(Collections.singletonList(comment)));
        trip.setId(1);
        user.setUserID(2);
        trip.getPassengers().add(user);
        trip.getComments().add(comment);
        comment.setAuthor(user);
        trip.setStatus(TripStatus.DONE);
        tripService.getTripComments(1, 0, 4);
        Assert.assertEquals(1, tripService.getTripComments(1, 0, 4).getTotalElements());
    }

    @Test
    public void amIPassengerShouldReturnTrue() throws TripDoesntExistsException {
        when(tripRepository.findById(1)).thenReturn(trip);
        when(helper.getLoggedUsername()).thenReturn("passenger");
        when(userRepository.findByUsernameLikeAndEnabledTrue("passenger")).thenReturn(user);
        tripService.amIPassenger(1);
        verify(tripService, times(1)).amIPassenger(1);
    }

    @Test
    public void amIPassengerShouldReturnFalse() throws TripDoesntExistsException {
        when(tripRepository.findById(1)).thenReturn(trip);
        when(helper.getLoggedUsername()).thenReturn("passenger");
        when(userRepository.findByUsernameLikeAndEnabledTrue("passenger")).thenReturn(user);
        tripService.amIPassenger(1);
        verify(tripService, times(1)).amIPassenger(1);
    }

    @Test(expected = TripDoesntExistsException.class)
    public void amIPassengerShouldThrowExceptionWhenTripDoesntExists() throws TripDoesntExistsException {
        tripService.amIPassenger(1);
    }

    @Test
    public void getAvailableLocationsShouldReturnLocation() {
        when(locationRepository.findAll()).thenReturn(Arrays.asList(origin, destination));
        tripService.getAvailableLocations();
        Assert.assertEquals(2, tripService.getAvailableLocations().size());
    }
}

