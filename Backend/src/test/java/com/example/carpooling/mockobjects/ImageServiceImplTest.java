package com.example.carpooling.mockobjects;

import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.ImageDoesntExistException;
import com.example.carpooling.exceptions.NotAccessingOwnResources;
import com.example.carpooling.exceptions.UserDoesntExistsException;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.ImageRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.services.ImageServiceImpl;
import com.example.carpooling.utils.Helper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.util.Date;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceImplTest {

    @Mock
    ImageRepository imageRepository;

    @Mock
    private CarRepository carRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Helper helper;


    @Spy
    @InjectMocks
    private ImageServiceImpl imageService;

    private User user;
    private MockMultipartFile mockMultipartFile;
    private Date date;
    private Car car;
    private Image image;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.image = new Image("data".getBytes());
        this.mockMultipartFile = new MockMultipartFile("data", "data", "data", "data".getBytes());
        UserDetails userDetails = new UserDetails("pesho", "petrov", "email@abv.bg", "0888888888", image, date, date);
        this.user = new User("name", "password", true, userDetails);
        this.car = new Car("audi a5", 4, true, false, 2007, user, date, date);
    }

    @Test
    public void uploadUserAvatarShouldUpload() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        imageService.uploadUserAvatar(mockMultipartFile, 1);
        verify(imageService, times(1)).uploadUserAvatar(mockMultipartFile, 1);
    }

    @Test(expected = UserDoesntExistsException.class)
    public void uploadUserAvatarShouldThrowExceptionWhenUserDoesntExists() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(null);
        imageService.uploadUserAvatar(mockMultipartFile, 1);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void uploadUserAvatarShouldThrowExceptionWhenAttemptsToChangeNotOwnsResources() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(false);
        imageService.uploadUserAvatar(mockMultipartFile, 1);
    }

    @Test
    public void getUserAvatarShouldReturnAvatar() throws ImageDoesntExistException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        user.getUserDetails().setImage(image);
        imageService.getUserAvatar(1);
        verify(imageService, times(1)).getUserAvatar(1);
    }

    @Test(expected = UserDoesntExistsException.class)
    public void getUserAvatarShouldThrowExceptionWhenUserDoesntExists() throws ImageDoesntExistException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(null);
        imageService.getUserAvatar(1);
    }

    @Test(expected = ImageDoesntExistException.class)
    public void getUserAvatarShouldThrowExceptionWhenImageDoesntExists() throws ImageDoesntExistException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        user.getUserDetails().setImage(null);
        imageService.getUserAvatar(1);
    }

    @Test
    public void getCarImageShouldReturnImage() throws CarDoesntExistsException, ImageDoesntExistException {
        when(carRepository.findById(1)).thenReturn(car);
        car.getCarImages().add(image);
        image.setId(1);
        car.setId(1);
        imageService.getCarImage(1, 1);
        verify(imageService, times(1)).getCarImage(1, 1);
    }

    @Test(expected = CarDoesntExistsException.class)
    public void getCarImageShouldThrowExceptionWhenCarDoesntExists() throws CarDoesntExistsException, ImageDoesntExistException {
        when(carRepository.findById(1)).thenReturn(null);
        imageService.getCarImage(1, 1);
    }

    @Test(expected = ImageDoesntExistException.class)
    public void getCarImageShouldThrowExceptionWhenImageDoesntExists() throws CarDoesntExistsException, ImageDoesntExistException {
        when(carRepository.findById(1)).thenReturn(car);
        imageService.getCarImage(1, 1);
    }

    @Test
    public void getCarImagesShouldReturnImages() throws CarDoesntExistsException {
        when(carRepository.findById(1)).thenReturn(car);
        car.getCarImages().add(image);
        image.setId(1);
        car.setId(1);
        when(helper.constructCarImageURL(1, 1)).thenReturn("carImage");
        imageService.getCarImages(1);
        verify(imageService, times(1)).getCarImages(1);
    }

    @Test(expected = CarDoesntExistsException.class)
    public void getCarImagesShouldThrowExceptionWhenCarDoesntExists() throws CarDoesntExistsException {
        when(carRepository.findById(1)).thenReturn(null);
        imageService.getCarImages(1);
    }

}
