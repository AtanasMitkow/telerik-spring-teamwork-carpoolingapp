package com.example.carpooling.mockobjects;


import com.example.carpooling.DTO.CreateCarDTO;
import com.example.carpooling.DTO.EditCarDTO;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.services.CarServiceImpl;
import com.example.carpooling.utils.Helper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceImplTest {

    @Mock
    private CarRepository carRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Helper helper;

    @Spy
    @InjectMocks
    private CarServiceImpl carService;

    private Date date;
    private Car car;
    private User user;
    private CreateCarDTO createCarDTO;
    private EditCarDTO editCarDTO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Image image = new Image();
        UserDetails userDetails = new UserDetails("pesho", "petrov", "email@abv.bg", "0888888888", image, date, date);
        this.user = new User("name", "password", true, userDetails);
        this.editCarDTO = new EditCarDTO(1, "audi a5", 4, true, 2007, "");
        this.createCarDTO = new CreateCarDTO("audi a5", 4, true, 2017);
        this.car = new Car("audi a5", 4, true, false, 2007, user, date, date);
    }

    @Test
    public void createShouldCreate() throws UserDoesntExistsException, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {

        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        carService.create(createCarDTO);
        Mockito.verify(carService, Mockito.times(1)).create(createCarDTO);
    }

    @Test(expected = UserDoesntExistsException.class)
    public void createShouldThrowException() throws UserDoesntExistsException, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {

        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(null);
        carService.create(createCarDTO);
    }

    @Test
    public void updateCarShouldUpdate() throws CarDoesntExistsException, NotAccessingOwnResources, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {

        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(car);
        car.setId(1);
        user.getOwnedCars().add(car);
        carService.update(editCarDTO);
        Mockito.verify(carService, Mockito.times(1)).update(editCarDTO);

    }

    @Test(expected = CarDoesntExistsException.class)
    public void updateCarShouldThrowExceptionWhenCarDoesntExists() throws CarDoesntExistsException, NotAccessingOwnResources, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {

        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(null);
        car.setId(1);
        user.getOwnedCars().add(car);
        carService.update(editCarDTO);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void updateCarShouldThrowExceptionWhenUSerNotAccessingOwnResources() throws CarDoesntExistsException, NotAccessingOwnResources, CarModelLengthException, CarYearMadeException, CarSeatsNumberException {

        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(car);
        car.setId(1);
        carService.update(editCarDTO);

    }

    @Test
    public void deleteOwnCarShouldDelete() throws CarDoesntExistsException, NotAccessingOwnResources {
        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(car);
        car.setId(1);
        user.getOwnedCars().add(car);
        carService.deleteOwnCar(1);
        Assert.assertTrue(car.isDeleted());
    }

    @Test(expected = CarDoesntExistsException.class)
    public void deleteOwnCarShouldThrowExceptionWhenCarDoesntExist() throws CarDoesntExistsException, NotAccessingOwnResources {
        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(null);
        user.getOwnedCars().add(car);
        carService.deleteOwnCar(1);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void deleteOwnCarShouldThrowExceptionWhenNotAccessingOwnResources() throws CarDoesntExistsException, NotAccessingOwnResources {
        when(helper.getLoggedUsername()).thenReturn("name");
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findById(1)).thenReturn(car);
        car.setId(1);
        carService.deleteOwnCar(1);
    }

    @Test
    public void getAllByOwnerShouldReturnCarsWithoutFilter() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        user.setUserID(1);
        when(helper.getLoggedUsername()).thenReturn(user.getUsername());
        Pageable pageable = PageRequest.of(0, 8, Sort.by("id"));
        List<Car> cars = Collections.singletonList(car);
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(carRepository.findByOwnerAndDeletedFalse(pageable, user)).thenReturn(new PageImpl<>(cars));
        carService.getAllByOwner(0, 8);
        Assert.assertEquals(1, carService.getAllByOwner(0, 8).getTotalElements());
    }

    @Test(expected = UserDoesntExistsException.class)
    public void getAllByOwnerShouldThrowExceptionWhenUserDoesntExists() throws NotAccessingOwnResources, UserDoesntExistsException {
        carService.getAllByOwner(0, 8);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void getAllByOwnerShouldThrowExceptionWhenNotAccessingOwnResources() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(false);
        user.setUserID(1);
        when(helper.getLoggedUsername()).thenReturn(user.getUsername());
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        carService.getAllByOwner(0, 8);
    }

    @Test
    public void getAllShouldReturnCars() {
        Pageable pageable = PageRequest.of(0, 8);
        when(carRepository.findAll(pageable)).thenReturn(new PageImpl<>(Collections.singletonList(car)));
        carService.getAll(0, 8);
        Assert.assertEquals(1, carService.getAll(0, 8).getTotalElements());
    }

    @Test
    public void getCarByIDShouldReturnCar() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(helper.getLoggedUsername()).thenReturn(user.getUsername());
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        when(carRepository.findById(1)).thenReturn(car);
        car.setId(1);
        carService.getCarByID(1);
        Assert.assertEquals(car, carService.getCarByID(1));
    }

    @Test(expected = UserDoesntExistsException.class)
    public void getCarByIDShouldThrowExceptionWhenUserDoesntExists() throws NotAccessingOwnResources, UserDoesntExistsException {
        carService.getCarByID(1);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void getCarByIDShouldThrowExceptionWhenNotAccessingOwnResources() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(helper.getLoggedUsername()).thenReturn(user.getUsername());
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(false);
        carService.getCarByID(1);
    }

}
