package com.example.carpooling.mockobjects;

import com.example.carpooling.DTO.CreateUserDTO;
import com.example.carpooling.DTO.LoginDTO;
import com.example.carpooling.DTO.UserDTO;
import com.example.carpooling.config.jwt_config.JwtTokenProvider;
import com.example.carpooling.exceptions.*;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.models.utility.JWTBlacklist;
import com.example.carpooling.repositories.JWTBlacklistRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.services.RatingService;
import com.example.carpooling.services.UserServiceImpl;
import com.example.carpooling.utils.Helper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private Helper helper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    @Mock
    private RatingService ratingService;

    @Mock
    private JWTBlacklistRepository jwtBlacklistRepository;

    @Spy
    @InjectMocks
    private UserServiceImpl userService;

    private User user;
    private CreateUserDTO createUserDTO;
    private Date date;
    private UserDTO userDTO;
    private Car car;
    private LoginDTO loginDTO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Image image = new Image();
        UserDetails userDetails = new UserDetails("pesho", "petrov", "emailche@abv.bg", "0888888888", image, date, date);
        this.user = new User("name", "password", true, userDetails);
        this.userDTO = new UserDTO(1, "name", "pesho", "petrov", "emailche@abv.bg", "0888888888", 2, 2, "");
        this.car = new Car("audi a5", 4, true, false, 2007, user, date, date);
        this.loginDTO = new LoginDTO("name", "password", true);
        this.createUserDTO = new CreateUserDTO("name", "pesho", "petrov", "emailche@abv.bg", "password", "0888888888");
    }

    @Test
    public void registerShouldCreateUser() throws InvalidPhoneNumberException, UsernameAlreadyExistsException, InvalidPasswordException, NameLengthException {
        when(userRepository.findByUsernameAndEnabledTrue(user.getUsername())).thenReturn(null);
        when(helper.isValidPassword(user.getPassword())).thenReturn(true);
        when(passwordEncoder.encode(user.getPassword())).thenReturn(user.getPassword());
        when(helper.isValidPhoneNumber("0888888888")).thenReturn(true);
        userService.registerUser(createUserDTO);
        verify(userService, times(1)).registerUser(createUserDTO);
    }

    @Test
    public void editUserShouldEdit() throws NotAccessingOwnResources, InvalidPhoneNumberException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        when(helper.isValidPhoneNumber("0888888888")).thenReturn(true);
        userService.editUser(userDTO);
        verify(userService, times(1)).editUser(userDTO);
    }

    @Test(expected = UserDoesntExistsException.class)
    public void editUserShouldThrowExceptionWhenUserDoesntExists() throws NotAccessingOwnResources, InvalidPhoneNumberException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(null);
        userService.editUser(userDTO);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void editUserShouldThrowExceptionWhenAttempsToChangeNowOwnsREsources() throws NotAccessingOwnResources, InvalidPhoneNumberException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(false);
        userService.editUser(userDTO);
    }

    @Test(expected = InvalidPhoneNumberException.class)
    public void editUserShouldThrowExceptionWhenPPhoneNumberIsNotValid() throws NotAccessingOwnResources, InvalidPhoneNumberException, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        when(helper.isValidPhoneNumber("0888888888")).thenReturn(false);
        userService.editUser(userDTO);
    }

    @Test
    public void getByUsernameShouldReturnUser() throws UserDoesntExistsException {
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(user);
        when(ratingService.getAVGDriverRating(user)).thenReturn(3.2);
        when(ratingService.getAVGPassengerRating(user)).thenReturn(4.2);

        userService.getByUsername("name");
        verify(userService, times(1)).getByUsername("name");
    }

    @Test(expected = UserDoesntExistsException.class)
    public void getByUsernameShouldThrowExceptionWhenUserDoesntExists() throws UserDoesntExistsException {
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(null);
        userService.getByUsername("name");
    }

    @Test
    public void getOwnedCarsShouldReturnList() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(true);
        user.getOwnedCars().add(car);
        userService.getOwnedCars(1);
        Assert.assertEquals(1, user.getOwnedCars().size());
    }

    @Test(expected = UserDoesntExistsException.class)
    public void getOwnedCarsShouldThrowExceptionWhenUSerDoesntExists() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(null);
        userService.getOwnedCars(1);
    }

    @Test(expected = NotAccessingOwnResources.class)
    public void getOwnedCarsShouldThrowExceptionWhenNotAccessingOwnResources() throws NotAccessingOwnResources, UserDoesntExistsException {
        when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        when(helper.attemptsToChangeOwnsResources(user)).thenReturn(false);
        userService.getOwnedCars(1);
    }

    @Test
    public void loginShouldLogin() throws UserDoesntExistsException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(userRepository.findByUsernameAndEnabledTrue("name")).thenReturn(user);
        userService.login(loginDTO);
        verify(userService, times(1)).login(loginDTO);
    }

    @Test(expected = NullPointerException.class)
    public void logoutShouldLogout() throws NotLoggedInException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(userService.getAuthCookie(request)).thenThrow(new NullPointerException());
        JWTBlacklist blackList = mock(JWTBlacklist.class);
        when(jwtTokenProvider.resolveToken(request)).thenReturn("token");
        when(jwtBlacklistRepository.save(blackList)).thenReturn(null);
        userService.logout(request);
        verify(userService, times(1)).logout(request);
    }

    @Test
    public void getAllShouldReturnUsers() {
        Pageable pageable = PageRequest.of(0, 10, Sort.by("username"));
        when(userRepository.findAllByEnabledIsTrue(pageable)).thenReturn(new PageImpl<>(Collections.singletonList(user)));
        when(ratingService.getAVGPassengerRating(user)).thenReturn(3.1);
        when(ratingService.getAVGDriverRating(user)).thenReturn(2.7);
        userService.getAll(0, 10);
        verify(userService, times(1)).getAll(0, 10);
    }
}

