package com.example.carpooling.mockobjects;

import com.example.carpooling.exceptions.CarDoesntExistsException;
import com.example.carpooling.exceptions.TripDoesntExistsException;
import com.example.carpooling.exceptions.UserDoesntExistsException;
import com.example.carpooling.models.Car;
import com.example.carpooling.models.Trip;
import com.example.carpooling.models.User;
import com.example.carpooling.models.UserDetails;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.models.utility.Location;
import com.example.carpooling.models.utility.TripStatus;
import com.example.carpooling.repositories.CarRepository;
import com.example.carpooling.repositories.TripRepository;
import com.example.carpooling.repositories.UserRepository;
import com.example.carpooling.services.AdminServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplTest {

    @Mock
    private CarRepository carRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TripRepository tripRepository;

    @Spy
    @InjectMocks
    private AdminServiceImpl adminService;

    private User user;
    private UserDetails userDetails;
    private Car car;
    private Trip trip;
    private Date date;
    private Image image;
    private Location origin;
    private Location destination;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.image = new Image();
        this.origin = new Location("Yambol");
        this.destination = new Location("Sofia");
        this.userDetails = new UserDetails("pesho", "petrov", "email@abv.bg", "0888888888", image, date, date);
        this.user = new User("name", "password", true, userDetails);
        this.car = new Car("audi a5", 4, true, false, 2007, user, date, date);
        this.trip = new Trip("message", 4, date, TripStatus.AVAILABLE, true, true, true, 200, false, user, car, origin, destination, date, date, 10);

    }

    @Test
    public void deleteUserShouldDelete() throws UserDoesntExistsException {
        Mockito.when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(user);
        adminService.deleteUser(1);
        Assert.assertFalse(user.isEnabled());
    }

    @Test
    public void deleteCarShouldDelete() throws CarDoesntExistsException {
        Mockito.when(carRepository.findById(1)).thenReturn(car);
        adminService.deleteCar(1);
        Assert.assertTrue(car.isDeleted());
    }

    @Test
    public void deleteTripShouldDelete() throws TripDoesntExistsException {
        Mockito.when(tripRepository.findById(1)).thenReturn(trip);
        adminService.deleteTrip(1);
        Assert.assertTrue(trip.isDeleted());
    }

    @Test(expected = UserDoesntExistsException.class)
    public void deleteUserShouldThrowException() throws UserDoesntExistsException {
        Mockito.when(userRepository.findByUserIDAndEnabledTrue(1)).thenReturn(null);
        adminService.deleteUser(1);

    }

    @Test(expected = CarDoesntExistsException.class)
    public void deleteCarShouldThrowException() throws CarDoesntExistsException {
        Mockito.when(carRepository.findById(1)).thenReturn(null);
        adminService.deleteCar(1);
    }

    @Test(expected = TripDoesntExistsException.class)
    public void deleteTripShouldThrowException() throws TripDoesntExistsException {
        Mockito.when(tripRepository.findById(1)).thenReturn(null);
        adminService.deleteTrip(1);
    }


}
