package com.example.carpooling.mockobjects;

import com.example.carpooling.exceptions.RatingRangeException;
import com.example.carpooling.models.*;
import com.example.carpooling.models.utility.Image;
import com.example.carpooling.models.utility.Location;
import com.example.carpooling.models.utility.TripStatus;
import com.example.carpooling.repositories.RatingRepository;
import com.example.carpooling.services.RatingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTest {

    @Mock
    RatingRepository ratingRepository;

    @Spy
    @InjectMocks
    RatingServiceImpl ratingService;

    private HashMap<User, Double> avgDriverRatings;
    private HashMap<User, Double> avgPassengerRatings;
    private User rater;
    private User rated;
    private Trip trip;
    private Date date;
    private Car car;
    private Location origin;
    private Location destination;
    private Rating rating;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Image image = new Image();
        avgDriverRatings = new HashMap<>();
        avgPassengerRatings = new HashMap<>();
        rating = new Rating(rater, rated, trip, 4.3, 1.5, date, date);
        this.origin = new Location("Yambol");
        this.destination = new Location("Sofia");
        this.car = new Car("golf 4", 4, true, false, 2007, rated, date, date);
        this.trip = new Trip("message", 4, date, TripStatus.AVAILABLE, true, true, true, 200, false, rated, car, origin, destination, date, date, 20);
        UserDetails userDetails = new UserDetails("pesho", "petrov", "email@abv.bg", "0888888888", image, date, date);
        this.rater = new User("name", "password", true, userDetails);
        this.rated = new User("name1", "password", true, userDetails);
    }

    @Test
    public void rateDriverShouldRate() throws RatingRangeException {
        Mockito.when(ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip)).thenReturn(rating);
        ratingService.rateDriver(rater, rated, trip, 4.5);
        Mockito.verify(ratingService, Mockito.times(1)).rateDriver(rater, rated, trip, 4.5);
    }

    @Test
    public void rateDriverShouldRate2() throws RatingRangeException {
        Mockito.when(ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip)).thenReturn(null);
        ratingService.rateDriver(rater, rated, trip, 4.5);
        Mockito.verify(ratingService, Mockito.times(1)).rateDriver(rater, rated, trip, 4.5);
    }

    @Test
    public void ratePassengerShouldRate() throws RatingRangeException {
        Mockito.when(ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip)).thenReturn(rating);
        ratingService.ratePassenger(rater, rated, trip, 4.5);
        Mockito.verify(ratingService, Mockito.times(1)).ratePassenger(rater, rated, trip, 4.5);
    }

    @Test
    public void ratePassengerShouldRate2() throws RatingRangeException {
        Mockito.when(ratingRepository.findRatingByRaterAndRatedAndTrip(rater, rated, trip)).thenReturn(null);
        ratingService.ratePassenger(rater, rated, trip, 4.5);
        Mockito.verify(ratingService, Mockito.times(1)).ratePassenger(rater, rated, trip, 4.5);
    }

    @Test
    public void getAvgDriverRatingShouldReturnRating() {
        Mockito.when(ratingRepository.calculateAVGDriverRating(rated)).thenReturn(5.0);
        ratingService.getAVGDriverRating(rated);
        double delta = 1e-15;
        Assert.assertEquals(5.0, ratingService.getAVGDriverRating(rated), delta);
    }

    @Test
    public void getAvgDriverRatingShouldReturnRating2() {
        Mockito.when(ratingRepository.calculateAVGDriverRating(rated)).thenReturn(5.0);
        avgDriverRatings.put(rated, 5.0);
        double delta = 1e-15;
        ratingService.getAVGDriverRating(rated);
        Assert.assertEquals(5.0, avgDriverRatings.get(rated), delta);
    }

    @Test
    public void getAvgPassengerRatingShouldReturnRating() {
        Mockito.when(ratingRepository.calculateAVGPassengerRating(rated)).thenReturn(5.0);
        ratingService.getAVGPassengerRating(rated);
        double delta = 1e-15;
        Assert.assertEquals(5.0, ratingService.getAVGPassengerRating(rated), delta);
    }

    @Test
    public void getAvgPassengerRatingShouldReturnRating2() {
        Mockito.when(ratingRepository.calculateAVGPassengerRating(rated)).thenReturn(5.0);
        avgPassengerRatings.put(rated, 5.0);
        double delta = 1e-15;
        ratingService.getAVGPassengerRating(rated);
        Assert.assertEquals(5.0, avgPassengerRatings.get(rated), delta);
    }

    @Test
    public void getDriversLeaderBoardsShouldReturnPage() {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = Arrays.asList(rated, rated);
        Mockito.when(ratingRepository.findByOrderByRatedAsDriver(pageable)).thenReturn(new PageImpl<>(users));
        ratingService.getDriversLeaderBoards(0, 10);
        Assert.assertEquals(2, ratingService.getDriversLeaderBoards(0, 10).getTotalElements());

    }

    @Test
    public void getPassengersLeaderBoardsShouldReturnPage() {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = Arrays.asList(rated, rated);
        Mockito.when(ratingRepository.findByOrderByRatedAsPassenger(pageable)).thenReturn(new PageImpl<>(users));
        ratingService.getPassengersLeaderBoards(0, 10);
        Assert.assertEquals(2, ratingService.getPassengersLeaderBoards(0, 10).getTotalElements());

    }
}
