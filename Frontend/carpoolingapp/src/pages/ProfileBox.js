import React from 'react'
import '../styles/profilebox.css'
import axios from 'axios';
import StarRatings from 'react-star-ratings';
import DefaultAvatar from '../images/defaultavatar.png'
import Modal from 'react-modal'
import Notifications from "./Notifications"

class ProfileBox extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state=
        {
            avatarURL: DefaultAvatar,
            notifications: 0,
            modal: false
        }

        this.loadNotificationCount = this.loadNotificationCount.bind(this)
        this.getAvatar = this.getAvatar.bind(this)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.viewNotifications = this.viewNotifications.bind(this)
    }

    componentDidMount()
    {
        this.getAvatar()
        this.loadNotificationCount();
        this.interval = setInterval(this.loadNotificationCount, 60000);  

    }

    getAvatar()
    {
        axios.get(this.props.userData.userDetails.avatarUri)
        .then(()=>this.setState({avatarURL: this.props.userData.userDetails.avatarUri}))
        .catch(error=>{})
    }

    async loadNotificationCount()
    {
        const response = await axios.get(`http://localhost:8080/api/users/notifications?page=0&size=4`)
        if(response.data.content[0].unread !== undefined && this.state.notifications !== response.data.content[0].unread)
        {
            this.setState({
                notifications: response.data.content[0].unread
            })
        }
    }

    viewNotifications()
    {
        axios.put("http://localhost:8080/api/users/notifications")
        .then(response=>{
            this.setState({
                notifications: 0
            })
        })
        .catch(error=>{})
    }

    componentWillUnmount() {
        clearInterval(this.interval);
      }

      closeModal() {
        this.setState({modal: false},this.viewNotifications);
      }

      openModal()
      {
          this.setState({modal:true});
      }


    render()
    {
        return(
            <div className="profilebox">
            {this.state.notifications === 0 ? null : <button onClick={this.openModal} className="notifications">{this.state.notifications}</button>}
            <img src={this.state.avatarURL} alt="profile"></img>
            <p>{this.props.userData.userDetails.firstName} {this.props.userData.userDetails.lastName}</p>
            <h3>Driver rating</h3>
            <div className="rating">
            <StarRatings
            rating={this.props.userData.userDetails.ratingAsDriver}
            starRatedColor="#0a91ca"
            numberOfStars={5}
            starDimension="20px"
            name='ratingAsDriver'
            />
            </div>
            <h3>Passenger rating</h3>
            <div className="rating">
            <StarRatings
            rating={this.props.userData.userDetails.ratingAsPassenger}
            starRatedColor="#0a91ca"
            numberOfStars={5}
            starDimension="20px"
            name='ratingAsPassenger'
            />
            </div>
            <Modal
                isOpen={this.state.modal}
                onRequestClose={this.closeModal}
                shouldCloseOnEsc={true}
                shouldCloseOnOverlayClick={true}
                contentLabel="Notifications"
                style={customStyles}
                >
                <h1>Notifications</h1>
                <Notifications/>

                </Modal>
            </div>
        )
    }


}

Modal.setAppElement("#root")
const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      width                 : '40%',
      height                : '60%',
      padding               : '1% 0',
      textAlign             : 'center',
    }
  };



export default ProfileBox;