import React, { Fragment } from 'react'
import {Link} from 'react-router-dom'

export default function PassengerRow(props)
{
    const doneButtons = props.passenger.status === "ACCEPTED" || props.passenger.status === "ABSENT" ? 
    <td><button onClick={()=>props.openModal(props.passenger.id)} className="accept">Rate</button>
    <button onClick={()=>props.changeStatus(props.passenger.id,"ABSENT")}className="decline">Absent</button></td>
    :
    null

    const actionButtons = props.tripStatus === "AVAILABLE" || props.tripStatus === "BOOKED" ?
    <td><button onClick={()=>props.changeStatus(props.passenger.id,"ACCEPTED")} className="accept">Accept</button>
    <button onClick={()=>props.changeStatus(props.passenger.id,"REJECTED")}className="decline">Reject</button></td>
    :
    <Fragment>{doneButtons}</Fragment>

    return(
        <tr className="pRow">
        <td>{props.passenger.username}</td>
        <td>{props.passenger.firstName} {props.passenger.lastName}</td>
        <td>{props.passenger.ratingAsPassenger}</td>
        <td>{props.passenger.status}</td>
        <td><Link to={`/profile/${props.passenger.username}`}><button className="viewProfile">View profile</button></Link></td>
        {actionButtons}
    </tr>
    )
}