import React from 'react'
import moment from 'moment'
import {Link} from 'react-router-dom'

export default function TableRows(props)
{
    const tripID = props.tripData.id;
    let date = moment(props.tripData.departureTime).format("MMMM Do YYYY, HH:mm")
    return(
        <tr>
            <td>{date}</td>
            <td>{props.tripData.origin}</td>
            <td>{props.tripData.destination}</td>
            <td>{props.tripData.status}</td>
            <td>{props.tripData.availablePlaces}</td>
            <td className="small"><Link to={`/trips/${tripID}`}><button>View</button></Link></td>
        </tr>
    )
}