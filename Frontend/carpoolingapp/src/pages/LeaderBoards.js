import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import "../styles/leaderBoards.css";
import InfiniteScroll from 'react-infinite-scroll-component'
import { NavLink ,Link} from 'react-router-dom';


class LeaderBoards extends React.Component {

    constructor(props) {
        super(props)
        this.state =
            {
                users: [],
                shallUpdate: false,
                userType: 'drivers',
                currentPage: -1,
                hasMore: true,
                currentItem:0
            }

        this.getUsers = this.getUsers.bind(this)
    }

    componentDidUpdate() {
        if (this.state.shallUpdate) {
            this.getUsers(this.props.match.params.userType)
        }
    }

    componentDidMount() {

        const userType = this.props.match.params.userType
        this.getUsers(userType)
    }

    static getDerivedStateFromProps(props, state) {
        if (props.match.params.userType !== state.userType) {
            return {
                shallUpdate: true,
                users: [],
                userType: props.match.params.userType,
                currentPage: -1,
                currentItem: 0,
                
            }

        }
        else return null
    }

    getUsers(userType) {
        const self = this;
        const page = this.state.currentPage + 1;
        axios.get(`http://localhost:8080/api/users/${this.state.userType}?page=${page}&size=10`)
            .then(response => {
                let i = this.state.currentItem;
                self.setState({
                    users: this.state.users.concat(response.data.content.map(user=>{
                        i = i +1
                        return {position: i,userData: user}
                    })),
                    shallUpdate: false,
                    hasMore: !response.data.last,
                    currentPage: page,
                    currentItem: i
                })

            })
    }
    render() {
    
        
        return (
            <div className="App">
                <div className="App__Aside-Left"></div>
                <div className="App__Aside">
                    <div className="PageSwitcher pageSwitcher">
                        <NavLink to="/leaderboards/drivers" activeClassName="PageSwitcher__Item--Active"
                            className="PageSwitcher__Item">Drivers</NavLink>
                        <NavLink to="/leaderboards/passengers" activeClassName="PageSwitcher__Item--Active"
                            className="PageSwitcher__Item">Passengers</NavLink>
                    </div>


                    <Paper className="lb table">
                        <InfiniteScroll
                            className="infiniteScroller padding-top"
                            dataLength={this.state.users.length}
                            next={this.getUsers}
                            hasMore={this.state.hasMore}
                            loader={<div className="loader" key={0}>Loading ...</div>}
                            height={580}
                        >
                            <Table className="root">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>№</TableCell>
                                        <TableCell align="center">Full Name</TableCell>
                                        <TableCell align="center">Username</TableCell>
                                        <TableCell align="center">Rating</TableCell>
                                        <TableCell align="center">Info</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>

                                    {this.state.users.map(user => {
                                        const rating=  this.state.userType === "drivers" ?  user.userData.ratingAsDriver:
                                        user.userData.ratingAsPassenger
                                       return (
                                        <TableRow key={user.userData.id} height="50px">
                                            <TableCell className="num" scope="row" >
                                            {user.position}
                                            </TableCell>

                                            <TableCell align="left">
                                                <div className="lb">{user.userData.firstName} {user.userData.lastName}</div>
                                            </TableCell>
                                            <TableCell align="center">{user.userData.username}</TableCell>
                                            

                                            <TableCell align="center" className="ratingRow">{rating}</TableCell>
                                            <TableCell align="center" className="ratingRow">
                                            
                                                <Link to={`/profile/${user.userData.username}`}><button className="button">View</button></Link>
                                                </TableCell>
                                        </TableRow>
                                    )})}

                                </TableBody>
                            </Table>
                        </InfiniteScroll>
                    </Paper>
                </div>
            </div>
        );
    }
}

export default LeaderBoards
