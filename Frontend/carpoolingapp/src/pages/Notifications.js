import React from 'react'
import Axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component'
import Notification from './Notification'

class Notifications extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state=
        {
            hasMore: false,
            currentPage: -1,
            notifications: []
        }

        this.getNotifications = this.getNotifications.bind(this)
    }

    componentDidMount()
    {
        this.getNotifications()
    }

    getNotifications()
    {
        const page = this.state.currentPage + 1;
        Axios.get(`http://localhost:8080/api/users/notifications?page=${page}&size=6`)
        .then(response=>
            {
                this.setState({
                    hasMore: !response.data.last,
                    currentPage: page,
                    notifications: this.state.notifications.concat(response.data.content)
                })
            })
    }

    render()
    {
        return(
            <InfiniteScroll
                                dataLength={this.state.notifications.length}
                                next ={this.getNotifications}
                                hasMore={this.state.hasMore}
                                loader={<h3>Loading..</h3>}
                                height={300}
                                scrollThreshold={0.95}
                            >   
                                {this.state.notifications.length !== 0 ?
                                this.state.notifications.map(not=>(
                                <Notification key={not.id} notification={not}/>
                                )): <h5 className="noComments">No notifications to show</h5>}
                                </InfiniteScroll>
        )
    }
}



export default Notifications;