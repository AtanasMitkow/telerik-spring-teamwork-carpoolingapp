import React from 'react'
import TripImage from "../images/trip-img.png"
import "../styles/trips.css"
import moment from 'moment'
import {Link} from 'react-router-dom'
export default function Trip(props)
{
    let date = moment(props.tripData.departureTime).format("MMMM Do YYYY, HH:mm")
    const estimatedTime = props.tripData.estimatedTime === -1 ? "Unknown" : props.tripData.estimatedTime + "min"
    const length = props.tripData.length === -1 ? "Unknown" : props.tripData.length + "km"
    return (
    <div className="tripContainer">
    <div className="tripCard fade-in">
    <div className="tripInfo">
    
    <div className="loc">
        <h5>from:</h5>
        <h3><b>{props.tripData.origin}</b></h3>
      </div>
      <div className="loc">
        <h5>to:</h5>
        <h3><b>{props.tripData.destination}</b></h3>
      </div>

    <h4>{date}</h4>
    <p>Driver: <b>{props.tripData.driver.firstName} {props.tripData.driver.lastName}</b></p>
    <p>Driver rating: <b>{props.tripData.driver.ratingAsDriver}</b></p>
    <p>Available places:<b> {props.tripData.availablePlaces}</b></p>
    <p>Estimated time:<b>{estimatedTime} </b></p>
    <p>Estimated length:<b> {length}</b></p>
    <Link to={`/trips/${props.tripData.id}`}><button className="tripInfoButton">Trip info</button></Link>
    <Link to={`/profile/${props.tripData.driver.username}`}><button className="tripInfoButton">Driver info</button></Link>
    </div>
    <img src={TripImage} alt="top"></img>
      
    </div>
  </div>
  )
}
