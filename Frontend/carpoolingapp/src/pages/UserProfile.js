import React from 'react'
import "../styles/App.css"
import "../styles/forms.css"
import "../styles/profile.css"
import "../styles/table.css"
import StarRatings from 'react-star-ratings'
import TableRows from './TableComponents/TableRows'
import InfiniteScroll from 'react-infinite-scroll-component'
import DefaultAvatar from '../images/defaultavatar.png'
import axios from 'axios';
import Select from 'react-select'
import {Link} from 'react-router-dom'

class UserProfile extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = 
        {
            shallIUpdate: false,
            userDetails: {},
            trips: [],
            hasMore: false,
            currentPage: -1,
            destinationCriteria: "",
            originCriteria: "",
            statusCriteria: "",
            avatarURL: DefaultAvatar,
            availableLocations: [],
            statusOptions:[{value: "AVAILABLE", label: "Available" },{value: "BOOKED", label: "Booked" },{value: "ONGOING", label: "Ongoing" },
      {value: "DONE", label: "Done" },{value: "CANCELED", label: "Cancelled" }]
            
        }
        this.validateAndGetAvatar = this.validateAndGetAvatar.bind(this)
        this.getTrips = this.getTrips.bind(this)
        this.getLocations = this.getLocations.bind(this)
    }

     componentDidMount()
    {
        const username = this.props.match.params.username;
        this.getProfile(username)
        this.getTrips()
        this.getLocations()  
    }

    getLocations()
    {
      let self = this;
      axios.get("http://localhost:8080/api/trips/locations")
        .then(function(response)
        {
            let options = []
            options =response.data.map(location => ({ value: location.name, label: location.name }))
            self.setState({availableLocations: self.state.availableLocations.concat(options)});
        })
    }

    getProfile(username)
    {
        axios.get(`http://localhost:8080/api/users/${username}`)
        .then(response =>
            {
                this.setState({
                    userDetails: response.data,
                    shallIUpdate: false
                },this.validateAndGetAvatar)
            })
        
    }

    filterTrips()
    {
      let self = this
      self.setState({
        currentPage: -1,
        trips: [],
        hasMore: false
      },this.getTrips
      )
    }

    handleOriginChange = selectedOption=>  {
        this.setState({
            originCriteria: selectedOption === null ? "" : selectedOption.value
        })
        this.filterTrips()
      };
  
      handleDestinationChange = selectedOption=>  {
        this.setState({
            destinationCriteria: selectedOption === null ? "" : selectedOption.value
        })
        this.filterTrips()
      };
  
      handleStatusChange = selectedOption=>  {
        this.setState({
            statusCriteria: selectedOption === null ? "" : selectedOption.value
        })
        this.filterTrips()
      };

    getTrips()
    {
        const self = this
        const {originCriteria,destinationCriteria,statusCriteria} = this.state
        const page = this.state.currentPage + 1
        axios.get(`http://localhost:8080/api/trips?page=${page}&size=8&driver=${this.props.match.params.username}&origin=${originCriteria}&destination=${destinationCriteria}&status=${statusCriteria}`)
        .then(response=>{
            self.setState({
                trips: self.state.trips.concat(response.data.content),
                hasMore: !response.data.last,
                currentPage: page 
            })
        })
    }


    static getDerivedStateFromProps(props, state) {
        if (props.match.params.username !== state.userDetails.username && state.userDetails.username !== undefined) {
          return {
            shallIUpdate: true,
            trips: [],
            currentPage: -1,
            hasMore: false
          }
        }
                  else return null

    }

    componentDidUpdate()
    {
        if(this.state.shallIUpdate)
        {
            this.getProfile(this.props.match.params.username)
            this.getTrips()
            this.validateAndGetAvatar()
        }
    }

    validateAndGetAvatar()
    {
        axios.get(this.state.userDetails.avatarUri)
        .then(response =>{
            this.setState({
                avatarURL: this.state.userDetails.avatarUri
            })
        }).catch(error=>{})
    }

    render()
    {
        const isMyProfile =this.props.loggedUsername === this.props.match.params.username
        const editButton = isMyProfile ? <Link to={`/profile/${this.props.match.params.username}/edit`}><button className="bottomButton">Edit Profile</button></Link> : null
        return(
            <div className="App">
                <div className="App__Aside-Left"></div>
                <div className="App__Aside">
                    <div className="createForm">
                    <div className="header"><h3>User profile</h3></div>
                        <div className="top">
                            <div className="profile-picture-container">
                                <img className="profile-picture" src={this.state.avatarURL} alt="profile"></img>
                            </div>
                            <div className="information">
                                <h5>fullname</h5>
                                <h3>{this.state.userDetails.firstName} {this.state.userDetails.lastName}</h3>
                                <h5>username</h5>
                                <h3>{this.state.userDetails.username}</h3>
                                <h5>phone</h5>
                                <h3>{this.state.userDetails.phone}</h3>
                                <h5>e-mail</h5>
                                <h3>{this.state.userDetails.email}</h3>
    
                            </div>
                            <div className="ratings">
                                <h1>{this.state.userDetails.ratingAsDriver}</h1>
                                <StarRatings
                                rating={this.state.userDetails.ratingAsDriver}
                                starRatedColor="#0a91ca"
                                numberOfStars={5}
                                starDimension="20px"
                                name='ratingAsDriver'
                                />
                                <h3>Driver</h3>
                            </div>
                            <div className="ratings">
                                <h1>{this.state.userDetails.ratingAsPassenger}</h1>
                                <StarRatings
                                rating={this.state.userDetails.ratingAsPassenger}
                                starRatedColor="#0a91ca"
                                numberOfStars={5}
                                starDimension="20px"
                                name='ratingAsPassenger'
                                />
                                <h3>Passenger</h3>
                            </div>

                        </div>
                        <div className="bottom visible-scroll">
                            <InfiniteScroll
                            className="visible-scroll"
                                dataLength={this.state.trips.length}
                                next ={this.getTrips}
                                hasMore={this.state.hasMore}
                                loader={<h3>Loading..</h3>}
                                height={370}
                            >
                            
                            <table>
                                <tbody>
                                <tr className="fixed">
                                <th>When</th>
                                <th>
                                <Select 
                                    options={this.state.availableLocations} 
                                     onChange={this.handleOriginChange}
                                    isClearable={true}
                                    placeholder="Filter by origin"/> 
                                </th>
                                <th>
                                <Select 
                                    options={this.state.availableLocations}
                                    onChange={this.handleDestinationChange}
                                    isClearable={true} 
                                    placeholder="Filter by destination"/> 
                                </th>
                                <th>
                                    <Select 
                                    options={this.state.statusOptions}
                                    isClearable={true}
                                    onChange={this.handleStatusChange}
                                    placeholder="Filter by status"/>                       
                                </th>
                                <th>Passengers</th>
                                <th></th>
                                </tr>
                                
                                {this.state.trips.length !== 0 ? this.state.trips.map(trip=>(
                                    <TableRows key={trip.id} tripData={trip}/>
                                )) : <tr><td><h5 className="nothing">No trips to show</h5></td></tr>}
                                </tbody>
                            </table>
                            </InfiniteScroll>
                            
                        </div>
                        {editButton}
                    </div>
                </div>

            </div>
        )

    }
}



export default UserProfile;
