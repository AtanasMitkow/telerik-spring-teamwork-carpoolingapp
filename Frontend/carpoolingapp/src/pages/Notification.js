import React from 'react'
import "../styles/profilebox.css"
import {NavLink} from 'react-router-dom'

export default function Notification(props)
{
    console.log(props.notification)
    const newNotification = props.notification.read ? null : <div className="newNotification">NEW!</div>
    return(
        <NavLink className="noStyling" to={`/trips/${props.notification.trip.id}`}><div className="notification">
            {newNotification}
               {props.notification.message}
        </div></NavLink>
    )
}