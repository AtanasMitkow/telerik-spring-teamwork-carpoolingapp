# Telerik Spring Teamwork CarPoolingApp

This is a project for a car pooling application for ShipCars and our Telerik Academy final project.

Trello board:
https://trello.com/b/YrDVDtUm

Database diagram:
![Diagram](Frontend/carpoolingapp/src/images/CarpoolingApp-database.png)
